# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-02-24 03:01+0300\n"
"Last-Translator: Alexander Potashev <aspotashev@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_userManual.jpg"
msgstr ""

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_tutorials.jpg"
msgstr ""

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_getting_started.jpg"
msgstr ""

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_reference.jpg"
msgstr ""

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_general.jpg"
msgstr ""

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_faq.jpg"
msgstr ""

#: ../../index.rst:0
msgid ".. image:: images/en/Hero_resources.jpg"
msgstr ""

#: ../../index.rst:5
msgid "Welcome to the Krita |version| Manual!"
msgstr "Добро пожаловать в руководство по Krita |version|!"

#: ../../index.rst:7
msgid ""
"Welcome to Krita's documentation page. Krita is a sketching and painting "
"program designed for digital artists."
msgstr ""

#: ../../index.rst:9
msgid ""
"As you learn about Krita, keep in mind that it is not intended as a "
"replacement for Photoshop. This means that the other programs may have more "
"features than Krita for image manipulation tasks, such as stitching together "
"photos, while Krita's tools are most relevant to digital painting, concept "
"art, illustration, and texturing. This fact accounts for a great deal of "
"Krita's design."
msgstr ""

#: ../../index.rst:11
msgid "`The manual as epub <https://docs.krita.org/en/epub/KritaManual.epub>`_"
msgstr ""

#: ../../index.rst:16
msgid ":ref:`user_manual`"
msgstr ":ref:`user_manual`"

#: ../../index.rst:16
msgid ":ref:`tutorials`"
msgstr ""

#: ../../index.rst:18
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""

#: ../../index.rst:18
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""

#: ../../index.rst:24
msgid ":ref:`getting_started`"
msgstr ""

#: ../../index.rst:24
msgid ":ref:`reference_manual`"
msgstr ""

#: ../../index.rst:26
msgid "New to Krita and don't know where to start?"
msgstr ""

#: ../../index.rst:26
msgid "A quick run-down of all of the tools that are available"
msgstr ""

#: ../../index.rst:31
msgid ":ref:`general_concepts`"
msgstr ""

#: ../../index.rst:31
msgid ":ref:`faq`"
msgstr ""

#: ../../index.rst:33
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""

#: ../../index.rst:33
msgid ""
"Find answers to the most common questions about Krita and what it offers."
msgstr ""

#: ../../index.rst:38
msgid ":ref:`resources_page`"
msgstr ""

#: ../../index.rst:38
msgid ":ref:`genindex`"
msgstr ""

#: ../../index.rst:40
msgid ""
"Textures, brush packs, and python plugins to help add variety to your "
"artwork."
msgstr ""

#: ../../index.rst:40
msgid "An index of the manual for searching terms by browsing."
msgstr ""
