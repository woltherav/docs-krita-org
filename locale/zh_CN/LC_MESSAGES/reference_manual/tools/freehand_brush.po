msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-05 03:37+0200\n"
"PO-Revision-Date: 2019-04-09 18:49\n"
"Last-Translator: guoyunhe <i@guoyunhe.me>\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___freehand_brush.pot\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"

#: ../../reference_manual/tools/freehand_brush.rst:1
msgid ""
"Krita's freehand brush tool reference, containing how to use the stabilizer "
"in krita."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:17
msgid "Freehand Brush Tool"
msgstr "手绘笔刷工具"

#: ../../reference_manual/tools/freehand_brush.rst:19
msgid "|toolfreehandbrush|"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:21
msgid ""
"The default tool you have selected on Krita start-up, and likely the tool "
"that you will use the most."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:23
msgid ""
"The freehand brush tool allows you to paint on paint layers without "
"constraints like the straight line tool. It makes optimal use of your "
"tablet's input settings to control the brush-appearance. To switch the "
"brush, make use of the brush-preset docker."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:27
msgid "Hotkeys and Sticky keys"
msgstr "快捷键和粘滞键"

#: ../../reference_manual/tools/freehand_brush.rst:29
msgid "The freehand brush tool's hotkey is :kbd:`B`."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:31
msgid ""
"The alternate invocation is the ''color picker'' (standardly invoked by :kbd:"
"`Ctrl`) Press :kbd:`Ctrl` to switch the tool to \"color picker\", use left "
"or right click to pick fore and background color respectively. Release the :"
"kbd:`Ctrl` to return to the freehand brush tool."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:32
msgid ""
"The Primary setting is \"size\". (standardly invoked by :kbd:`Shift`) Press :"
"kbd:`Shift` and drag outward to increase brush size. Drag inward to decrease "
"it."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:33
msgid "You can also press :kbd:`V` as a stickykey for the straight-line tool."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:35
msgid ""
"The hotkey can be edited in :menuselection:`Settings --> Configure Krita --> "
"Configure Shortcuts`. The sticky-keys can be edited in :menuselection:"
"`Settings --> Configure Krita --> Canvas Input Settings`."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:39
msgid "Tool Options"
msgstr "工具选项"

#: ../../reference_manual/tools/freehand_brush.rst:45
msgid "Smoothing"
msgstr "平滑"

#: ../../reference_manual/tools/freehand_brush.rst:47
msgid ""
"Smoothing, also known as stabilising in some programs, allows the program to "
"correct the stroke. Useful for people with shaky hands, or particularly "
"difficult long lines."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:49
msgid "The following options can be selected:"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:51
msgid "No Smoothing."
msgstr "无平滑."

#: ../../reference_manual/tools/freehand_brush.rst:52
msgid ""
"The input from the tablet translates directly to the screen. This is the "
"fastest option, and good for fine details."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:53
msgid "Basic Smoothing."
msgstr "基本平滑."

#: ../../reference_manual/tools/freehand_brush.rst:54
msgid ""
"This option will smooth the input of older tablets like the Wacom Graphire "
"3. If you experience slightly jagged lines without any smoothing on, this "
"option will apply a very little bit of smoothing to get rid of those lines."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:56
msgid ""
"This option allows you to use the following parameters to make the smoothing "
"stronger or weaker:"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:58
#: ../../reference_manual/tools/freehand_brush.rst:70
msgid "Distance"
msgstr "距离"

#: ../../reference_manual/tools/freehand_brush.rst:59
msgid ""
"The distance the brush needs to move before the first dab is drawn. "
"(Literally the amount of events received by the tablet before the first dab "
"is drawn.)"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:60
msgid "Stroke Ending"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:61
msgid ""
"This controls how much the line will attempt to reach the last known "
"position of the cursor after the left-mouse button/or stylus is lifted. Will "
"currently always result in a straight line, so use with caution."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:62
msgid "Smooth Pressure"
msgstr "平滑压力"

#: ../../reference_manual/tools/freehand_brush.rst:63
msgid ""
"This will apply the smoothing on the pressure input as well, resulting in "
"more averaged size for example."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:65
msgid "Weighted smoothing:"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:65
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid "Scalable Distance"
msgstr "等比例距离"

#: ../../reference_manual/tools/freehand_brush.rst:65
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid ""
"This makes it so that the numbers involved will be scaled along the zoom "
"level."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:68
msgid ""
"This option averages all inputs from the tablet. It is different from "
"weighted smoothing in that it allows for always completing the line. It will "
"draw a circle around your cursor and the line will be a bit behind your "
"cursor while painting."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:71
msgid "This is the strength of the smoothing."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:72
msgid "Delay"
msgstr "推迟"

#: ../../reference_manual/tools/freehand_brush.rst:73
msgid ""
"This toggles and determines the size of the dead zone around the cursor. "
"This can be used to create sharp corners with more control."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:74
msgid "Finish Line"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:75
msgid "This ensures that the line will be finished."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:76
msgid "Stabilize sensors"
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:77
msgid ""
"Similar to :guilabel:`Smooth Pressure`, this allows the input (pressure, "
"speed, tilt) to be smoother."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:79
msgid "Stabilizer"
msgstr "防抖"

#: ../../reference_manual/tools/freehand_brush.rst:84
msgid "Assistants"
msgstr "助手"

#: ../../reference_manual/tools/freehand_brush.rst:86
msgid ""
"Ticking this will allow snapping to :ref:`assistant_tool`, and the hotkey to "
"toggle it is :kbd:`Ctrl + Shift + L`. See :ref:`painting_with_assistants` "
"for more information."
msgstr ""

#: ../../reference_manual/tools/freehand_brush.rst:88
msgid ""
"The slider will determine the amount of snapping, with 1000 being perfect "
"snapping, and 0 being no snapping at all. For situations where there is more "
"than one assistant on the canvas, the defaultly ticked :guilabel:`Snap "
"Single` means that Krita will only snap to a single assistant at a time, "
"preventing noise. Unticking it allows you to chain assistants together and "
"snap along them."
msgstr ""
