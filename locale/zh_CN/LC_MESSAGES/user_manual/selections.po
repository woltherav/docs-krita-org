msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-10 03:45+0200\n"
"PO-Revision-Date: 2019-04-09 18:49\n"
"Last-Translator: guoyunhe <i@guoyunhe.me>\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___selections.pot\n"

#: ../../<rst_epilog>:66
msgid ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"
msgstr ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: 矩形选区工具"

#: ../../<rst_epilog>:68
msgid ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"
msgstr ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: 椭圆选区工具"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: 多边形选区工具"

#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: 路径选区工具"

#: ../../<rst_epilog>:74
msgid ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"
msgstr ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: 轮廓选区工具"

#: ../../<rst_epilog>:76
msgid ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"
msgstr ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: 相邻选区工具"

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: 相似颜色选区工具"

#: ../../user_manual/selections.rst:1
msgid "How selections work in Krita."
msgstr "介绍 Krita 的选区工作方式。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`rectangle_selection_tool`"
msgstr ":ref:`rectangle_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectrect|"
msgstr "|toolselectrect|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a square."
msgstr "建立一个矩形选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`ellipse_selection_tool`"
msgstr ":ref:`ellipse_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectellipse|"
msgstr "|toolselectellipse|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a circle."
msgstr "建立一个椭圆形的选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`polygonal_selection_tool`"
msgstr ":ref:`polygonal_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../user_manual/selections.rst:1
msgid ""
"Click where you want each point of the Polygon to be. Double click to end "
"your polygon and finalize your selection area. Use :kbd:`Shift + Z` to undo "
"last point."
msgstr ""
"建立一个多边形的选区，在画布上每次点击可以建立一个节点，按 :kbd:`Shift + Z` "
"可以撤销回上一个点的位置，双击将自动将首尾两点相连，封闭该多边形选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`outline_selection_tool`"
msgstr ":ref:`outline_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectoutline|"
msgstr "|toolselectoutline|"

#: ../../user_manual/selections.rst:1
msgid ""
"Outline/Lasso tool is used for a rough selection by drawing the outline."
msgstr "在别的软件里有时称作“套索工具”，通过手绘轮廓建立一个选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`similar_selection_tool`"
msgstr ":ref:`similar_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectsimilar|"
msgstr "|toolselectsimilar|"

#: ../../user_manual/selections.rst:1
#, fuzzy
#| msgid "Similar Color Selection Tool"
msgid "Similar Color Selection Tool."
msgstr "按照颜色的相似程度建立选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`contiguous_selection_tool`"
msgstr ":ref:`contiguous_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectcontiguous|"
msgstr "|toolselectcontiguous|"

#: ../../user_manual/selections.rst:1
msgid ""
"Contiguous or “Magic Wand” selects a field of color. Adjust the :guilabel:"
"`Fuzziness` to allow more changes in the field of color, by default limited "
"to the current layer."
msgstr ""
"在别的软件里有时叫做“魔棒”工具，以一块连续的颜色区域建立选区。通过 :guilabel:"
"`模糊度` 可以羽化选区边缘，默认仅对当前图层进行采样。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`path_selection_tool`"
msgstr ":ref:`path_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../user_manual/selections.rst:1
msgid ""
"Path select an area based on a vector path, click to get sharp corners or "
"drag to get flowing lines and close the path with :kbd:`Enter` or connecting "
"back to the first point."
msgstr ""
"通过一条矢量路径建立选区。直接点击创建直线，点击并拖动拉出曲线，按 :kbd:`回车"
"` 把路径首尾相连建立选区。"

#: ../../user_manual/selections.rst:17
msgid "Selections"
msgstr "选区"

#: ../../user_manual/selections.rst:19
msgid ""
"Selections allow you to pick a specific area of your artwork to change. "
"There are many selection tools available that select in different ways. Once "
"an area is selected, most tools will stay inside that area. On that area you "
"can draw or use gradients to quickly get colored and/or shaded shapes with "
"hard edges."
msgstr ""
"选区用于挑选并限定在一定的区域内更改图像。Krita 提供了许多种选区工具，可以用"
"不同的方式建立选区。在建立一个选区之后，绝大多数的工具只能在该选区内部生效。"
"你可以在选区内部大笔勾勒和使用填充渐变，它们并不会影响到选区外面的画面，但有"
"时会留下坚硬的边缘。"

#: ../../user_manual/selections.rst:22
msgid "Creating Selections"
msgstr "创建选区"

#: ../../user_manual/selections.rst:24
msgid ""
"The most common selection tools all exist at the bottom of the toolbox. Each "
"tool selects things slightly differently. The links for each tool go into a "
"more detailed description of how to use it."
msgstr ""
"你可以在工具箱的最下面一组找到最常用的一系列选区工具。每种选区工具用一种独特"
"的方式进行选择。在下表点击每个工具的名称可以跳传到该工具的详细介绍页面。"

#: ../../user_manual/selections.rst:38
msgid ""
"You can also use the transform tools on your selection, a great way to try "
"different proportions on parts of your image."
msgstr "你还可以在选区里面使用变形工具，这是一个调整画面不同部分比例的好办法。"

#: ../../user_manual/selections.rst:41
msgid "Editing Selections"
msgstr "编辑选区"

#: ../../user_manual/selections.rst:43
msgid ""
"The tool options for each selection tool gives you the ability to modify "
"your selection."
msgstr ""
"每个选区工具的工具选项都给出了选项来更改选区。你还可以使用下列快捷键来提高工"
"作效率："

#: ../../user_manual/selections.rst:47
msgid "Action"
msgstr "操作"

#: ../../user_manual/selections.rst:47
msgid "Modifier"
msgstr "修饰键 (临时切换)"

#: ../../user_manual/selections.rst:47
msgid "Shortcut"
msgstr "快捷键 (永久切换)"

#: ../../user_manual/selections.rst:49
msgid "Replace"
msgstr "替换"

#: ../../user_manual/selections.rst:49
msgid "Ctrl"
msgstr "Ctrl"

#: ../../user_manual/selections.rst:49
msgid "R"
msgstr "R"

#: ../../user_manual/selections.rst:51
msgid "Intersect"
msgstr "相交"

#: ../../user_manual/selections.rst:51
msgid "Shift + Alt"
msgstr "Shift + Alt"

#: ../../user_manual/selections.rst:51
msgid "--"
msgstr "无"

#: ../../user_manual/selections.rst:53
msgid "Add"
msgstr "添加"

#: ../../user_manual/selections.rst:53
msgid "Shift"
msgstr "Shift"

#: ../../user_manual/selections.rst:53
msgid "A"
msgstr "A"

#: ../../user_manual/selections.rst:55
msgid "Subtract"
msgstr "减去"

#: ../../user_manual/selections.rst:55
msgid "Alt"
msgstr "Alt"

#: ../../user_manual/selections.rst:55
msgid "S"
msgstr "S"

#: ../../user_manual/selections.rst:59
msgid "Removing Selections"
msgstr "移除选区"

#: ../../user_manual/selections.rst:61
msgid ""
"If you want to delete the entire selection, the easiest way is to deselect "
"everything. :menuselection:`Select --> Deselect`. Shortcut :kbd:`Ctrl + "
"Shift + A`."
msgstr ""
"要移除整个选区，可以使用取消选择功能，它在菜单栏的 :menuselection:`选择 --> "
"取消选择` ，你也可以使用快捷键 :kbd:`Ctrl + Shift + A` 。"

#: ../../user_manual/selections.rst:64
msgid "Display Modes"
msgstr "选区显示模式"

#: ../../user_manual/selections.rst:66
msgid ""
"In the bottom left-hand corner of the status bar there is a button to toggle "
"how the selection is displayed. The two display modes are the following: "
"(Marching) Ants and Mask. The red color with Mask can be changed in the "
"preferences. You can edit the color under :menuselection:`Settings --> "
"Configure Krita --> Display --> Selection Overlay`. If there is no "
"selection, this button will not do anything."
msgstr ""
"位于窗口左下角，状态栏最左边的按钮用于显示和切换选区的显示模式。点击按钮可以"
"在蚂蚁线和蒙版两种模式之间切换。蒙版模式默认的红色可以在菜单栏的 :"
"menuselection:`设置 --> 配置 Krita --> 显示 --> 选区蒙版颜色` 。如果当前不存"
"在选区，则该按钮不会有任何作用。"

#: ../../user_manual/selections.rst:70
msgid ".. image:: images/en/Ants-displayMode.jpg"
msgstr ".. image:: images/en/Ants-displayMode.jpg"

#: ../../user_manual/selections.rst:71
msgid ""
"Ants display mode (default) is best if you want to see the areas that are "
"not selected."
msgstr ""
"蚂蚁线模式 (默认)，显示选区的大致轮廓，它不会干扰画面颜色，方便同时看清未被选"
"中的区域。"

#: ../../user_manual/selections.rst:74
msgid ".. image:: images/en/Mask-displayMode.jpg"
msgstr ".. image:: images/en/Mask-displayMode.jpg"

#: ../../user_manual/selections.rst:75
msgid ""
"Mask display mode is good if you are interested in seeing the various "
"transparency levels for your selection. For example, you can create a "
"selection with a gradient."
msgstr "蒙版模式可以揭示选区的透明度变化，例如那些通过渐变创建的选区。"

#: ../../user_manual/selections.rst:78
msgid "Global Selection Mask (Painting a Selection)"
msgstr "全局选区蒙版 (绘制选区)"

#: ../../user_manual/selections.rst:80
msgid ""
"The global Selection Mask is your selection that appears on the layers "
"docker. By default, this is hidden, so you will need to make it visible via :"
"menuselection:`Select --> Show Global Selection Mask`."
msgstr ""
"全局选区蒙版会把当前选区显示为在图层面板中的一个蒙版图层。在默认状态下它是隐"
"藏的，你可以在菜单栏的 :menuselection:`选择 --> 显示全局选区蒙版` 。"

#: ../../user_manual/selections.rst:83
msgid ".. image:: images/en/Global-selection-mask.jpg"
msgstr ".. image:: images/en/Global-selection-mask.jpg"

#: ../../user_manual/selections.rst:84
msgid ""
"Once the global Selection Mask is shown, you will need to create a "
"selection. The benefit of using this is that you can paint your selection "
"using any of the normal painting tools. The information is saved as "
"grayscale. You might want to switch to the Mask display mode if it is "
"difficult to see the results."
msgstr ""
"在全局选区蒙版显示功能启用之后，随便建立一个选区，接下来就可以在图层列表切换"
"到全局选区蒙版。这个模式的好处是你可以用任意一种常规绘画工具来绘制选区。该蒙"
"版的信息作为灰阶图像保存。要更好地把握所绘选区的效果，可以切换到蒙版显示模"
"式。"

#: ../../user_manual/selections.rst:90
msgid "Selection from layer transparency"
msgstr "按照图层透明度建立选区"

#: ../../user_manual/selections.rst:92
msgid ""
"You can create a selection based on a layer's transparency by right-clicking "
"on the layer in the layer docker and selecting :guilabel:`Select Opaque` "
"from the context menu."
msgstr ""
"要根据一个图层的透明度来创建一个选区，可在图层列表中右键单击一个图层，在右键"
"菜单中选择 :guilabel:`选择不透明区域`。"

#: ../../user_manual/selections.rst:95
msgid "Pixel and Vector Selection Types"
msgstr "像素和矢量选区类型"

#: ../../user_manual/selections.rst:97
msgid ""
"Vector selections allow you to modify your selection with vector anchor "
"tools. Pixel selections allow you to modify selections with pixel "
"information. They both have their benefits and disadvantages. You can "
"convert one type of selection to another."
msgstr ""
"矢量选区允许使用形状编辑工具来调整选区。像素选区允许对选区按像素信息进行修"
"改。这两种模式各有各的长处和短处。你可以把选区在两种模式之间进行转换。"

#: ../../user_manual/selections.rst:100
msgid ".. image:: images/en/Vector-pixel-selections.jpg"
msgstr ".. image:: images/en/Vector-pixel-selections.jpg"

#: ../../user_manual/selections.rst:101
msgid ""
"When creating a selection, you can select what type of selection you want "
"from the Mode in the selection tool options: Pixel or Vector."
msgstr ""
"在创建选区之前，你可以在选区工具的工具选项面板选择像素选区或者矢量选区模式。"

#: ../../user_manual/selections.rst:104
msgid ""
"Vector selections can modify as any other vector shape with the “Shape "
"Handle” tool, if you try to paint on a vector selection it will be converted "
"into a pixel selection. Pixel selections can be painted with any tool. You "
"can also convert vector shapes to selection. In turn, vector selections can "
"be made from vector shapes, and vector shapes can be converted to vector "
"selections using the options in the :guilabel:`Selection` menu. Krita will "
"add a new vector layer for this shape."
msgstr ""
"矢量选区和其他矢量形状一样可以通过形状编辑工具进行修改。如果你在矢量选区里进"
"行绘制，它会被转换成像素选区。像素选区里面可以用任意一种工具进行绘制。矢量形"
"状和矢量选区可以互相转换，可在选择菜单的 :guilabel:`转换为形状` 处进行转换。"

#: ../../user_manual/selections.rst:107
msgid ""
"One of the most common reasons to use vector selections is that they give "
"you the ability to move and transform a selection. Moving the selection with "
"a pixel selection will move the content on the layer. Moving the selection "
"on a vector selection will only move the selection. You can also use the "
"path editing tool to change the anchor points in the selection"
msgstr ""
"一般来说，使用矢量选区的好处是方便后期对选区本身进行移动或者变形。像素选区会"
"连带着选区的内容一起变形和移动，而矢量选区的变形和移动不会影响选区的内容。你"
"还可以使用形状编辑工具来更改矢量选区的各个锚点。"

#: ../../user_manual/selections.rst:115
msgid ""
"If you started with a pixel selection, you can still convert it to a vector "
"selection to get these benefits. Go to :menuselection:`Select --> Convert to "
"Vector Selection`."
msgstr ""
"如果你创建的是像素选区，你也可以把它转换成矢量选区来获得上述便利。要把像素选"
"区转换为矢量选区，可到菜单栏的 :menuselection:`选择 -->转换为矢量选区` 。"

#: ../../user_manual/selections.rst:119
msgid ""
"If you have multiple levels of transparency when you convert a selection to "
"vector, you will lose the gray values."
msgstr "将一个带有透明度变化的像素选区转换为矢量选区将丢失透明度的变化。"

#: ../../user_manual/selections.rst:122
msgid "Common Shortcuts while Using Selections"
msgstr "与选区功能配套使用的常用快捷键"

#: ../../user_manual/selections.rst:124
msgid "Copy -- :kbd:`Ctrl + C` or :kbd:`Ctrl + Ins`"
msgstr "复制 -- :kbd:`Ctrl + C` 或者 :kbd:`Ctrl + Ins`"

#: ../../user_manual/selections.rst:125
msgid "Paste -- :kbd:`Ctrl + V` or :kbd:`Shift + Ins`"
msgstr "粘贴 -- :kbd:`Ctrl + V` 或者 :kbd:`Shift + Ins`"

#: ../../user_manual/selections.rst:126
msgid "Cut -- :kbd:`Ctrl + X`, :kbd:`Shift + Del`"
msgstr "剪切 -- :kbd:`Ctrl + X`, :kbd:`Shift + Del`"

#: ../../user_manual/selections.rst:127
msgid "Copy From All Layers -- :kbd:`Ctrl + Shift + C`"
msgstr "从所有图层复制 -- :kbd:`Ctrl + Shift + C`"

#: ../../user_manual/selections.rst:128
msgid "Copy Selection to New Layer -- :kbd:`Ctrl + Alt + J`"
msgstr "把选区内容复制到新图层 -- :kbd:`Ctrl + Alt + J`"

#: ../../user_manual/selections.rst:129
msgid "Cut Selection to New Layer -- :kbd:`Ctrl + Shift + J`"
msgstr "把选区内容剪切到新图层 -- :kbd:`Ctrl + Shift + J`"

#: ../../user_manual/selections.rst:130
msgid "Display or hide selection with :kbd:`Ctrl + H`"
msgstr "切换显示/隐藏选区  --  :kbd:`Ctrl + H`"
