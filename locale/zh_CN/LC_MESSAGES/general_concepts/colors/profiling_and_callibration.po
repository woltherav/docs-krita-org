msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-17 03:39+0200\n"
"PO-Revision-Date: 2019-04-09 18:49\n"
"Last-Translator: guoyunhe <i@guoyunhe.me>\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___colors___profiling_and_callibration.pot\n"

#: ../../<generated>:1
msgid "soft-proofing"
msgstr "软打样"

#: ../../general_concepts/colors/profiling_and_callibration.rst:1
msgid "Color Models in Krita"
msgstr "Krita 的色彩模型"

#: ../../general_concepts/colors/profiling_and_callibration.rst:18
#, fuzzy
#| msgid "Profiling and Calibration:"
msgid "Profiling and Calibration"
msgstr "特性化和校准"

#: ../../general_concepts/colors/profiling_and_callibration.rst:20
msgid ""
"So to make it simple, a color profile is just a file defining a set of "
"colors inside a pure XYZ color cube. This \"color set\" can be used to "
"define different things:"
msgstr ""
"简单来说，色彩特性文件是一个在纯粹的 XYZ 色彩立方体中定义了一组颜色的文件。这"
"组颜色用于定义不同用途下的色彩："

#: ../../general_concepts/colors/profiling_and_callibration.rst:23
msgid "the colors inside an image"
msgstr "图像中的色彩"

#: ../../general_concepts/colors/profiling_and_callibration.rst:25
msgid "the colors a device can output"
msgstr "设备可以输出的色彩"

#: ../../general_concepts/colors/profiling_and_callibration.rst:27
msgid ""
"Choosing the right workspace profile to use depends on how much colors you "
"need and on the bit depth you plan to use. Imagine a line with the whole "
"color spectrum from pure black (0,0,0) to pure blue (0,0,1) in a pure XYZ "
"color cube. If you divide it choosing steps at a regular interval, you get "
"what is called a linear profile, with a gamma=1 curve represented as a "
"straight line from 0 to 1. With 8bit/channel bit depth, we have only 256 "
"values to store this whole line. If we use a linear profile as described "
"above to define those color values, we will miss some important visible "
"color change steps and have a big number of values looking the same (leading "
"to posterization effect)."
msgstr ""
"要选择合适的工作空间特性文件，取决于你所需颜色的多少和计划使用色彩通道位深度"
"大小。想象有一条线，它位于一个单纯 XYZ 色彩立方体内部，显示了从纯黑 (0,0,0) "
"到纯蓝 (0,0,1) 的一系列颜色。如果你用固定的间距将它分成几段，你就得到了一个线"
"性特性文件。gamma 为 1 的曲线就是一条从 0 到 1 的直线。在每通道为 8 位时，我"
"们只能在整条线上保存 256 个颜色数值。如果我们用上述的线性特性文件来定义这些颜"
"色，我们将丢失一些重要的颜色过渡，无论数值多少看上去都一样 (导致色调分离效"
"应)。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:33
msgid ""
"This is why was created the sRGB profile to fit more different colors in "
"this limited amount of values, in a perceptually regular grading, by "
"applying a custom gamma curve (see picture here: http://en.wikipedia.org/"
"wiki/SRGB) to emulate the standard response curve of old CRT screens. So "
"sRGB profile is optimized to fit all colors that most common screen can "
"reproduce in those 256 values per R/G/B channels. Some other profiles like "
"Adobe RGB are optimized to fit more printable colors in this limited range, "
"primarily extending cyan-green hues. Working with such profile can be useful "
"to improve print results, but is dangerous if not used with a properly "
"profiled and/or calibrated good display. Most common CMYK workspace profile "
"can usually fit all their colors within 8bit/channel depth, but they are all "
"so different and specific that it's usually better to work with a regular "
"RGB workspace first and then convert the output to the appropriate CMYK "
"profile."
msgstr ""
"这也是为什么我们设计了 sRGB 特性文件，它可以用有限的数值容纳更多不同的颜色。"
"sRGB 特性文件以人类感知上的等距为准进行级配，用一条据此修正的 gamma 曲线 (参"
"见：http://en.wikipedia.org/wiki/SRGB) 来模拟旧式 CRT 显示器的标准阶调响应曲"
"线。通过上述手段，sRGB 为容纳常见屏幕可显示的每 R/G/B 通道 256 种颜色进行了优"
"化。一些其他特性文件，如 AdobeRGB 等是为了在同样颜色数量下面容纳更多的可打印"
"颜色而优化的，它们一般会在色彩空间的青-绿方位进行延伸。使用宽色域进行作业可以"
"改善打印效果，但必须配合已校准并有正确特性文件的高品质显示器使用，否则极易造"
"成问题。绝大多数常见 CMYK 工作空间特性文件可以在每通道 8 位下面容纳它们的全部"
"颜色，但它们之间的差别很大，所以最好先用常规的 RGB 工作空间制作图像，然后在输"
"出时才转换至所需的 CMYK 特性文件。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:38
msgid ""
"Starting with 16bit/channel, we already have 65536 values instead of 256, so "
"we can use workspace profiles with higher gamut range like Wide-gamut RGB or "
"Pro-photo RGB, or even unlimited gamut like scRGB."
msgstr ""
"在把每通道位深度提升至 16 位时，我们便可以容纳 65536 个数值，比 8 位的 256 个"
"要多得多。因此在 16 位或更高位深度下面我们可以使用色域更宽的工作空间特性文"
"件，如 Wide-gamut RGB 或 Pro-photo RGB，甚至无限色域的空间，如 scRGB。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:40
msgid ""
"But sRGB being a generic profile (even more as it comes from old CRT "
"specifications...), there are big chances that your monitor have actually a "
"different color response curve, and so color profile. So when you are using "
"sRGB workspace and have a proper screen profile loaded (see next point), "
"Krita knows that the colors the file contains are within the sRGB color "
"space, and converts those sRGB values to corresponding color values from "
"your monitor profile to display the canvas."
msgstr ""
"但 sRGB 毕竟只是一个通用特性文件 (它来自旧式 CRT 规范)，你的显示器还有它的色"
"彩特性文件的颜色响应曲线可能与 sRGB 不符。因此在使用 sRGB 工作空间时可以加载"
"正确的显示器特性文件，Krita 会按 sRGB 空间解读文件所含颜色，然后把那些颜色数"
"值按照显示器特性文件进行转换，最后显示到画布上。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:43
msgid ""
"Note that when you export your file and view it in another software, this "
"software has to do two things:"
msgstr "要注意的是，在你导出了文件并用另一个软件查看它时，该软件应该做两件事："

#: ../../general_concepts/colors/profiling_and_callibration.rst:45
msgid ""
"read the embed profile to know the \"good\" color values from the file "
"(which most software do nowadays; when they don't they usually default to "
"sRGB, so in the case described here we're safe )"
msgstr ""
"读取嵌入的特性文件以判断哪些颜色可以显示 (绝大多数当代软件会做这一步，即使它"
"们不做，也往往会默认文件为 sRGB，这在我们当前描述的情景下是安全的)"

#: ../../general_concepts/colors/profiling_and_callibration.rst:46
msgid ""
"and then convert it to the profile associated to the monitor (which very few "
"software actually does, and just output to sRGB.. so this can explain some "
"viewing differences most of the time)."
msgstr ""
"然后把文件的颜色按照显示器的特性文件进行转换 (极少软件会做这一步，大多数只会"
"输出成 sRGB，因此会造成观察效果不一致)。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:48
msgid "Krita uses profiles extensively, and comes bundled with many."
msgstr "Krita 会尽可能地使用特性文件，并随软件预装了大量特性文件。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:50
msgid ""
"The most important one is the one of your own screen. It doesn't come "
"bundled, and you have to make it with a color profiling device. In case you "
"don't have access to such a device, you can't make use of Krita's color "
"management as intended. However, Krita does allow the luxury of picking any "
"of the other bundled profiles as working spaces."
msgstr ""
"你的显示器的特性文件是最重要的一个特性文件。它不随软件预装，你必须使用校色设"
"备自行制作。如果你缺少该设备，你将无法按照软件的设计意图来使用 Krita 的色彩管"
"理功能。但不管怎样，你依然可以在 Krita 里面任选一种预装的特性文件作为工作空"
"间。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:54
msgid "Profiling devices"
msgstr "校色设备"

#: ../../general_concepts/colors/profiling_and_callibration.rst:56
msgid ""
"Profiling devices, called Colorimeters, are tiny little cameras of a kind "
"that you connect to your computer via an usb, and then you run a profiling "
"software (often delivered alongside of the device)."
msgstr ""
"校色设备，通常叫做色差计，可被看作是一种特殊的小型数码相机。要进行校色和特性"
"化，先把色差计通过 USB 连接到计算机，然后再运行特性化软件 (一般随该设备提"
"供)。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:60
msgid ""
"If you don't have software packaged with your colorimeter, or are unhappy "
"with the results, we recommend `Argyllcms <http://www.argyllcms.com/>`_"
msgstr ""
"如果你的色差没有自带软件，或者你不满意自带软件的结果，我们推荐使用 "
"`Argyllcms <http://www.argyllcms.com/>`_"

#: ../../general_concepts/colors/profiling_and_callibration.rst:62
#, fuzzy
#| msgid ""
#| "The little camera then measures what the brightest red, green, blue, "
#| "white and black are like on your screen using a predefined white as base. "
#| "It also measures how grey the color grey is."
msgid ""
"The little camera then measures what the brightest red, green, blue, white "
"and black are like on your screen using a predefined white as base. It also "
"measures how gray the color gray is."
msgstr ""
"色差计的感光元件会在一种预设白的基础上测量最纯的红、绿、蓝、白、黑在屏幕上的"
"实际显示效果，它也会测量中间灰的实际效果。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:64
msgid ""
"It then puts all this information into an ICC profile, which can be used by "
"the computer to correct your colors."
msgstr ""
"上述信息会被存入一个 ICC 特性文件，计算机便可以使用该文件来校准颜色了。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:66
msgid ""
"It's recommended not to change the \"calibration\" (contrast, brightness, "
"you know the menu) of your screen after profiling. Doing so makes the "
"profile useless, as the qualities of the screen change significantly while "
"calibrating."
msgstr ""
"我们不建议在进行特性化之后更改显示器的显示参数 (对比度、亮度等)。一旦更改，之"
"前制作的特性文件就失去了意义，因为屏幕的显示效果已明显变化。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:68
msgid ""
"To make your screen display more accurate colors, you can do one or two "
"things: profile your screen or calibrate and profile it."
msgstr ""
"要让你的显示器更加精确地显示颜色，你可以有两种校色办法：仅进行特性化或校准并"
"进行特性化。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:71
#, fuzzy
#| msgid ""
#| "Just profiling your screen means measuring the colors of your monitor "
#| "with its native settings. and put those values in a color profile, which "
#| "can be used by color-managed application to adapt source colors to the "
#| "screen for optimal result. Calibrating and profiling means the same "
#| "except that first you try to calibrate the screen colors to match a "
#| "certain standard setting like sRGB or other more specific profiles. "
#| "Calibrating is done first with hardware controls (lightness, contrast, "
#| "gamma curves), and then with software that creates a vcgt (video card "
#| "gamma table) to load in the GPU."
msgid ""
"Just profiling your screen means measuring the colors of your monitor with "
"its native settings and put those values in a color profile, which can be "
"used by color-managed application to adapt source colors to the screen for "
"optimal result. Calibrating and profiling means the same except that first "
"you try to calibrate the screen colors to match a certain standard setting "
"like sRGB or other more specific profiles. Calibrating is done first with "
"hardware controls (lightness, contrast, gamma curves), and then with "
"software that creates a vcgt (video card gamma table) to load in the GPU."
msgstr ""
"仅进行特性化意味着按照显示器的原生设置进行测量，测量得到的数值被存入一个色彩"
"特性文件，具有色彩管理能力的应用软件会按照这个文件来调整源颜色数值在该屏幕上"
"的最佳显示方式。校准并特性化意味着在进行特性化之前先把显示器调整得尽可能符合"
"某种规范，如符合 sRGB 或其他特性文件的规范。校准时首先调整显示器硬件的参数 "
"(亮度、对比、 gamma 曲线等)，然后用软件生成一个 vcgt (显卡 gamma 表，video "
"card gamma table)，加载到 GPU。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:75
msgid "So when or why should you do just one or both?"
msgstr "这两种方法我们应该如何选择呢？"

#: ../../general_concepts/colors/profiling_and_callibration.rst:77
msgid "Profiling only:"
msgstr "仅进行特性化："

#: ../../general_concepts/colors/profiling_and_callibration.rst:79
msgid "with a good monitor"
msgstr "高品质显示器"

#: ../../general_concepts/colors/profiling_and_callibration.rst:80
msgid ""
"you can get most of the sRGB colors and lot of extra colors not inside sRGB. "
"So this can be good to have more visible colors."
msgstr ""
"你可以得到 sRGB 空间内的绝大多数颜色，还能得到 sRGB 空间之外的许多颜色。更多"
"的可见颜色是件好事。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:82
msgid "with a bad monitor"
msgstr "低品质显示器"

#: ../../general_concepts/colors/profiling_and_callibration.rst:82
msgid ""
"you will get just a subset of actual sRGB, and miss lot of details, or even "
"have hue shifts. Trying to calibrate it before profiling can help to get "
"closer to full-sRGB colors."
msgstr ""
"你只能得到 sRGB 空间的一部分颜色，大量的颜色细节会丢失，甚至色相也会发生变"
"化。在制作配置文件之前校准显示器有助于得到更为接近完整 sRGB 空间的色彩。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:84
msgid "Calibration+profiling:"
msgstr "校准并进行特性化："

#: ../../general_concepts/colors/profiling_and_callibration.rst:86
msgid "bad monitors"
msgstr "低品质显示器"

#: ../../general_concepts/colors/profiling_and_callibration.rst:87
msgid "as explained just before."
msgstr "如上所述。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:88
msgid "multi-monitor setup"
msgstr "多显示器配置"

#: ../../general_concepts/colors/profiling_and_callibration.rst:89
msgid ""
"when using several monitors, and specially in mirror mode where both monitor "
"have the same content, you can't have this content color-managed for both "
"screen profiles. In such case, calibrating both screens to match sRGB "
"profile (or another standard for high-end monitors if they both support it) "
"can be a good solution."
msgstr ""
"在使用多个显示器时，尤其是在使用镜像模式在多个显示器上显示相同画面时，你无法"
"把内容同时用两个不同的特性文件进行色彩管理。在这种情况下，可把两个显示器按照 "
"sRGB 特性文件 (或者两个显示器同时支持的其他特性文件) 进行校准匹配。"

#: ../../general_concepts/colors/profiling_and_callibration.rst:91
msgid ""
"when you need to match an exact rendering context for soft-proofing, "
"calibrating can help getting closer to the expected result. Though switching "
"through several monitor calibration and profiles should be done extremely "
"carefully."
msgstr ""
"如果你需要为软打样匹配一模一样的渲染环境，显示器校准有助于得到更为接近的效"
"果。但在不同的显示器和特性文件之间来回切换时应当极为谨慎。"
