# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-03 03:22+0100\n"
"PO-Revision-Date: 2019-03-17 19:53+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:40
msgid ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:None
msgid ".. image:: images/en/Krita-multibrush.png"
msgstr ".. image:: images/en/Krita-multibrush.png"

#: ../../reference_manual/tools/multibrush.rst:1
msgid "Krita's multibrush tool reference."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:16
msgid "Multibrush Tool"
msgstr "Multipenselverktyg"

#: ../../reference_manual/tools/multibrush.rst:18
msgid "|toolmultibrush|"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:20
msgid ""
"The Multibrush tool allows you to draw using multiple instances of a "
"freehand brush stroke at once, it can be accessed from the Toolbox docker or "
"with the default shortcut :kbd:`Q`. Using the Multibrush is similar to "
"toggling the :ref:`mirror_tools`, but the Multibrush is more sophisticated, "
"for example it can mirror freehand brush strokes along a rotated axis."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:22
msgid "The settings for the tool will be found in the tool options dock."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:24
msgid ""
"The multibrush tool has three modes and the settings for each can be found "
"in the tool options dock. Symmetry and mirror reflect over an axis which can "
"be set in the tool options dock. The default axis is the center of the "
"canvas."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:29
msgid "The three modes are:"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:31
msgid "Symmetry"
msgstr "Symmetri"

#: ../../reference_manual/tools/multibrush.rst:32
msgid ""
"Symmetry will reflect your brush around the axis at even intervals. The "
"slider determines the number of instances which will be drawn on the canvas."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:33
msgid "Mirror"
msgstr "Spegla"

#: ../../reference_manual/tools/multibrush.rst:34
msgid "Mirror will reflect the brush across the X axis, the Y axis, or both."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:35
msgid "Translate"
msgstr "Förflytta"

#: ../../reference_manual/tools/multibrush.rst:36
msgid ""
"Translate will paint the set number of instances around the cursor at the "
"radius distance."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:38
msgid "Snowflake"
msgstr "Snöflinga"

#: ../../reference_manual/tools/multibrush.rst:38
msgid ""
"This works as a mirrored symmetry, but is a bit slower than symmetry+toolbar "
"mirror mode."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:40
msgid ""
"The assistant and smoothing options work the same as in the :ref:"
"`freehand_brush_tool`, though only on the real brush and not its copies."
msgstr ""
