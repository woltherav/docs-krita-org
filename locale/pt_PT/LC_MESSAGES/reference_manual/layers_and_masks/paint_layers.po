# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-24 03:29+0100\n"
"PO-Revision-Date: 2019-04-01 13:59+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Photoshop Krita guilabel kbd Ins rasterizações\n"
"X-POFile-SpellExtra: program bitmaps\n"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:1
msgid "How to use paint layers in Krita."
msgstr "Como usar as camadas de pintura no Krita."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:20
msgid "Paint Layers"
msgstr "Camadas de Pintura"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:22
msgid ""
"Paint layers are the most commonly used type of layers used in digital paint "
"or image manipulation software like Krita. If you've ever used layers in :"
"program:`Photoshop` or the :program:`Gimp`, you'll be used to how they work. "
"In short, a paint layer, also called a pixel, bitmap or raster layer, is a "
"bitmap image (an image made up of many points of color)."
msgstr ""
"As camadas de pintura são o tipo de camadas mais usados normalmente na "
"pintura digital ou nas aplicações de manipulação de imagens, como o Krita.  "
"Se alguma vez tiver usado camadas no :program:`Photoshop` ou no :program:"
"`Gimp`, estará habituado ao seu modo de funcionamento. Em resumo, uma camada "
"de pintura, também chamada de camada de pixels, imagens ou rasterizações, é "
"uma imagem de 'bitmaps' (uma imagem composta por muitos pontos de cores)."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:24
msgid ""
"Paint layers let you apply many advanced effects such as smearing, smudging "
"and distorting. This makes them the most flexible type of layer. However,  "
"paint layers don't scale well when enlarged (they pixelate), and any effects "
"that have been applied can't be edited."
msgstr ""
"As camadas de pintura permitem-lhe aplicar muitos efeitos avançados, como os "
"borrões, manchas e distorções. Isto torna-o o tipo de camadas mais flexível. "
"Contudo, as camadas de pintura não escalam muito bem quando são ampliadas "
"(criam efeitos de pixelização) e todos os efeitos que tenham sido aplicados "
"não poderão ser editados."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:26
msgid ""
"To deal with these two drawbacks, digital artists will typically work at "
"higher Pixel Per Inch (PPI) counts.  It is not unusual to see PPI settings "
"of 400 to 600 PPI for a canvas with a good amount of detail.  To combat the "
"issue of applied effects that cannot be edited it is best to take advantage "
"of the non-destructive layer capabilities of filter, transparency and "
"transform masks."
msgstr ""
"Para lidar com estas duas desvantagens, os artistas digitais irão "
"tipicamente trabalhar com valores de Pixels por Polegada (PPP) mais "
"elevados. É normal ver valores de PPP entre 400 e 600 PPP para uma área de "
"desenho com uma boa quantidade de detalhes. Para combater o problema dos "
"efeitos aplicados que não conseguem ser editados, é melhorar tirar partido "
"das capacidades não-destrutivas das máscaras de filtros, transparências e "
"transformações."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:28
msgid ""
"As long as you have enough resolution / size on your canvas though, and as "
"long as you aren't going to need to go back and tweak an effect you created "
"previously, then a paint layer is usually the type of layer you will want. "
"If you click on the :guilabel:`New layer` icon in the layers docker you'll "
"get a paint layer. Of course you can always choose the :guilabel:`New layer` "
"drop-down to get another type."
msgstr ""
"Desde que tenha uma resolução / tamanho suficientes na sua área de desenho, "
"e desde que não tenha de voltar atrás e afinar um efeito que tenha criado "
"anteriormente, então uma camada é normalmente o tipo de camada que irá "
"querer. Se carregar no ícone de :guilabel:`Nova camada` na área de camadas, "
"irá obter uma camada de pintura. Obviamente, poderá sempre escolher a lista :"
"guilabel:`Nova camada` para escolher outro tipo."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:30
msgid "The hotkey for adding a new paint layer is :kbd:`Ins`."
msgstr ""
"A combinação de teclas para adicionar uma nova camada de pintura é o :kbd:"
"`Ins`."
