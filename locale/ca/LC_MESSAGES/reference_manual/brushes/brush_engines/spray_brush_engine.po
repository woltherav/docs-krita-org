# Translation of docs_krita_org_reference_manual___brushes___brush_engines___spray_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-02 03:22+0100\n"
"PO-Revision-Date: 2019-04-18 17:02+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../<generated>:1
msgid "Mix with background color."
msgstr "Mescla amb el color de fons."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:1
msgid "The Spray Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell d'esprai."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:16
msgid "Spray Brush Engine"
msgstr "Motor del pinzell d'esprai"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:19
msgid ".. image:: images/icons/spraybrush.svg"
msgstr ".. image:: images/icons/spraybrush.svg"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:20
msgid "A brush that can spray particles around in its brush area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:23
msgid "Options"
msgstr "Opcions"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:25
msgid ":ref:`option_spray_area`"
msgstr ":ref:`option_spray_area`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:26
msgid ":ref:`option_spray_shape`"
msgstr ":ref:`option_spray_shape`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:27
msgid ":ref:`option_brush_tip` (Used as particle if spray shape is not active)"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:28
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:29
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:30
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:31
msgid ":ref:`option_shape_dyna`"
msgstr ":ref:`option_shape_dyna`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:32
msgid ":ref:`option_color_spray`"
msgstr ":ref:`option_color_spray`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:34
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:39
msgid "Spray Area"
msgstr "Àrea d'esprai"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:41
msgid "The area in which the particles are sprayed."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:43
msgid "Diameter"
msgstr "Diàmetre"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:44
msgid "The size of the area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:45
msgid "Aspect Ratio"
msgstr "Relació d'aspecte"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:46
msgid "It's aspect ratio: 1.0 is fully circular."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:47
msgid "Angle"
msgstr "Angle"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:48
msgid ""
"The angle of the spray size: works nice with aspect ratios other than 1.0."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:49
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:50
msgid "Scales the diameter up."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Spacing"
msgstr "Espaiat"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Increases the spacing of the diameter's spray."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:55
msgid "Particles"
msgstr "Partícules"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:57
msgid "Count"
msgstr "Comptador"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:58
msgid "Use a specified amount of particles."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:59
msgid "Density"
msgstr "Densitat"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:60
msgid "Use a % amount of particles."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:61
msgid "Jitter Movement"
msgstr "Moviment Jitter"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:62
msgid "Jitters the spray area around for extra randomness."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid "Gaussian Distribution"
msgstr "Distribució de Gauss"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid ""
"Focuses the particles to paint in the center instead of evenly random over "
"the spray area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:69
msgid "Spray Shape"
msgstr "Forma d'esprai"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:71
msgid ""
"If activated, this will generate a special particle. If not, the brush-tip "
"will be the particle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:74
msgid "Can be..."
msgstr "Pot ser..."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:76
msgid "Ellipse"
msgstr "El·lipse"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:77
msgid "Rectangle"
msgstr "Rectangle"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:78
msgid "Anti-aliased Pixel"
msgstr "Píxel amb antialiàsing"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:79
msgid "Pixel"
msgstr "Píxel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Shape"
msgstr "Forma"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Image"
msgstr "Imatge"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:82
msgid "Width & Height"
msgstr "Amplada i alçada"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:83
msgid "Decides the width and height of the particle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:84
msgid "Proportional"
msgstr "Proporcional"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:85
msgid "Locks Width & Height to be the same."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Texture"
msgstr "Textura"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Allows you to pick an image for the :guilabel:`Image shape`."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:92
msgid "Shape Dynamics"
msgstr "Dinàmiques de forma"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:94
msgid "Random Size"
msgstr "Mida aleatòria"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:95
msgid ""
"Randomizes the particle size between 1x1 px and the given size of the "
"particle in brush-tip or spray shape."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:96
msgid "Fixed Rotation"
msgstr "Gir fix"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:97
msgid "Gives a fixed rotation to the particle to work from."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:98
msgid "Randomized Rotation"
msgstr "Gir aleatori"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:99
msgid "Randomizes the rotation."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:100
msgid "Follow Cursor Weight"
msgstr "Estil de seguiment del cursor"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:101
msgid ""
"How much the pressure affects the rotation of the particles. At 1.0 and high "
"pressure it'll seem as if the particles are exploding from the middle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "Angle Weight"
msgstr "Estil d'angle"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "How much the spray area angle affects the particle angle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:108
msgid "Color Options"
msgstr "Opcions del color"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:110
msgid "Random HSV"
msgstr "HSV aleatori"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:111
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:112
msgid "Random Opacity"
msgstr "Opacitat aleatòria"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:113
msgid "Randomizes the opacity."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:114
msgid "Color Per Particle"
msgstr "Color per partícula"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:115
msgid "Has the color options be per particle instead of area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:116
msgid "Sample Input Layer."
msgstr "Capa d'entrada de la mostra."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:117
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:118
msgid "Fill Background"
msgstr "Emplena el fons"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:119
msgid "Fills the area before drawing the particles with the background color."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:121
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr ""
