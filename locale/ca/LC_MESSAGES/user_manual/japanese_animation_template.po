# Translation of docs_krita_org_user_manual___japanese_animation_template.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-10 03:45+0200\n"
"PO-Revision-Date: 2019-04-10 08:34+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../user_manual/japanese_animation_template.rst:1
msgid "Detailed explanation on how to use the animation template."
msgstr ""
"Explicació detallada sobre com utilitzar la plantilla per a l'animació."

#: ../../user_manual/japanese_animation_template.rst:17
msgid "Japanese Animation Template"
msgstr "Plantilla per a animació japonesa"

#: ../../user_manual/japanese_animation_template.rst:20
msgid ""
"This template is used to make Japanese-style animation. It is designed on "
"the assumption that it was used in co-production, so please customize its "
"things like layer folders according to scale and details of your works."
msgstr ""
"Aquesta plantilla s'utilitza per a crear animació a l'estil japonès. Està "
"dissenyada sota el supòsit que s'utilitzarà en coproducció, de manera que "
"personalitzeu les seves coses com les carpetes de capes segons l'escala i el "
"detall dels vostres treballs."

#: ../../user_manual/japanese_animation_template.rst:26
msgid "Basic structure of its layers"
msgstr "Estructura bàsica de les seves capes"

#: ../../user_manual/japanese_animation_template.rst:28
msgid ""
"Layers are organized so that your work will start from lower layers go to "
"higher layers, except for coloring layers."
msgstr ""
"Les capes estan organitzades de manera que el vostre treball comenci des de "
"les capes més baixes i vagi a les capes més altes, excepte per a donar color "
"a les capes."

#: ../../user_manual/japanese_animation_template.rst:32
msgid ".. image:: images/en/Layer_Organization.png"
msgstr ".. image:: images/en/Layer_Organization.png"

#: ../../user_manual/japanese_animation_template.rst:34
msgid "Its layer contents"
msgstr "Contingut de la capa"

#: ../../user_manual/japanese_animation_template.rst:36
msgid "from the bottom"
msgstr "des de baix"

#: ../../user_manual/japanese_animation_template.rst:38
msgid "Layout Paper"
msgstr "Paper per a la disposició"

#: ../../user_manual/japanese_animation_template.rst:39
msgid ""
"These layers are a form of layout paper. Anime tap holes are prepared on "
"separate layers in case you have to print it out and continue your drawing "
"traditionally."
msgstr ""
"Aquestes capes són un formulari de paper per a la disposició. En capes "
"separades, s'afegeixen orificis que seran útils per al cas que hàgiu "
"d'imprimir-ho i seguir dibuixant de la manera tradicional."

#: ../../user_manual/japanese_animation_template.rst:40
msgid "Layout (Background)"
msgstr "Disposició (fons)"

#: ../../user_manual/japanese_animation_template.rst:41
msgid ""
"These layers will contain background scenery or layouts which are scanned "
"from a traditional drawing. If you don't use them, you can remove them."
msgstr ""
"Aquestes capes contindran escenaris o disposicions de fons que s'escanegen a "
"partir d'un dibuix tradicional. Si no els utilitzeu, podeu eliminar-los."

#: ../../user_manual/japanese_animation_template.rst:42
msgid "Key drafts"
msgstr "Esbossos clau"

#: ../../user_manual/japanese_animation_template.rst:43
msgid "These layers are used to draw layouts digitally."
msgstr ""
"Aquestes capes s'utilitzen per a dibuixar disposicions a la manera digital."

#: ../../user_manual/japanese_animation_template.rst:44
msgid "Keys"
msgstr "Claus"

#: ../../user_manual/japanese_animation_template.rst:45
msgid ""
"Where you add some details to the layouts and arrange them to draw \"keys\" "
"of animation."
msgstr ""
"On afegireu alguns detalls a les disposicions i les organitzareu per a "
"dibuixar «claus» de l'animació."

#: ../../user_manual/japanese_animation_template.rst:46
msgid "Inbetweening"
msgstr "Entremig"

#: ../../user_manual/japanese_animation_template.rst:47
msgid ""
"Where you add inbetweens to keys for the process of coloring, and remove "
"unnecessary details to finalize keys (To be accurate, I finish finalization "
"of keys before beginning to add inbetweens)."
msgstr ""
"On s'afegeixen els entremigs a les claus per al procés de donar color i "
"elimina els detalls innecessaris per a finalitzar les claus (per a ser "
"precisos, he acabat la finalització de les claus abans de començar a afegir "
"els entremigs)."

#: ../../user_manual/japanese_animation_template.rst:48
msgid "Coloring (under Inbetweening)"
msgstr "Acolorir (sota l'entremig)"

#: ../../user_manual/japanese_animation_template.rst:49
msgid ""
"Where you fill areas with colors according to specification of inbetweens."
msgstr ""
"On s'emplenen les àrees amb colors d'acord a l'especificació dels entremigs."

#: ../../user_manual/japanese_animation_template.rst:50
msgid "Time Sheet and Composition sheet"
msgstr "Full de temps i full de composició"

#: ../../user_manual/japanese_animation_template.rst:51
msgid ""
"This contains a time sheet and composition sheet. Please rotate them before "
"using."
msgstr ""
"Això conté un full de temps i un full de composició. Si us plau, gireu-los "
"abans d'utilitzar."

#: ../../user_manual/japanese_animation_template.rst:53
msgid "Color set"
msgstr "Conjunt de colors"

#: ../../user_manual/japanese_animation_template.rst:53
msgid ""
"This contains colors used to draw main and auxiliary line art and fill "
"highlight or shadows. You can add them to your palette."
msgstr ""
"Això conté els colors utilitzats per a dibuixar la línia artística principal "
"i auxiliar, i omplir amb reflexos o ombres. Podeu afegir-los a la vostra "
"paleta."

#: ../../user_manual/japanese_animation_template.rst:56
msgid "Basic steps to make animation"
msgstr "Passos bàsics per a crear animació"

#: ../../user_manual/japanese_animation_template.rst:58
msgid ""
"Key draft --> assign them into Time sheet (or adjust them on Timeline, then "
"assign them into Time sheet) --> adjust them on Timeline --> add frames to "
"draw drafts for inbetweening if you need them --> Start drawing Keys"
msgstr ""
"Esbós clau --> assignar-lo a dins del full de temps (o ajustar-lo a la línia "
"de temps, després assignar-lo a dins del full de temps) --> ajustar-lo a la "
"línia de temps --> afegiu marcs per a dibuixar esbossos per a entremigs si "
"els necessiteu --> començar a dibuixar les claus"

#: ../../user_manual/japanese_animation_template.rst:61
msgid ".. image:: images/en/Keys_drafts.png"
msgstr ".. image:: images/en/Keys_drafts.png"

#: ../../user_manual/japanese_animation_template.rst:62
msgid "You can add layers and add them to timeline."
msgstr "Podeu afegir les capes i afegir-les a la línia de temps."

#: ../../user_manual/japanese_animation_template.rst:65
msgid ".. image:: images/en/Add_Timeline_1.png"
msgstr ".. image:: images/en/Add_Timeline_1.png"

#: ../../user_manual/japanese_animation_template.rst:67
msgid ".. image:: images/en/Add_Timeline_2.png"
msgstr ".. image:: images/en/Add_Timeline_2.png"

#: ../../user_manual/japanese_animation_template.rst:68
msgid ""
"This is due difference between 24 drawing per second, which is used in Full "
"Animation, and 12 drawing per second and 8 drawings per second, which are "
"used in Limited Animation, on the Timeline docker."
msgstr ""
"Aquesta és la deguda diferència entre 24 dibuixos per segon, el qual "
"s'utilitza en animació completa, i 12 dibuixos per segon i 8 dibuixos per "
"segon, els quals s'utilitzen en animació limitada a l'acoblador Línia de "
"temps."

#: ../../user_manual/japanese_animation_template.rst:71
msgid ".. image:: images/en/24_12_and_8_drawing_per_sec.png"
msgstr ".. image:: images/en/24_12_and_8_drawing_per_sec.png"

#: ../../user_manual/japanese_animation_template.rst:72
msgid ""
"This is correspondence between Timeline and Time sheet. \"Black\" layer is "
"to draw main line art which are used ordinary line art, \"Red\" layer is to "
"draw red auxiliary linearts which are used to specify highlights, \"Blue\" "
"layer is to draw blue auxiliary linearts which are used to specify shadows, "
"and \"Shadow\" layer is to draw light green auxiliary line art which are "
"used to specify darker shadows. However, probably you have to increase or "
"decrease these layers according to your work."
msgstr ""
"Aquesta és la correspondència entre la línia de temps i el full de temps. La "
"capa «Negra» és per a dibuixar la línia artística principal el qual "
"s'utilitza amb la línia artística habitual, la capa «Vermella» és per a "
"dibuixar la línia artística auxiliar en vermell, el qual s'utilitza per "
"especificar els reflexos, la capa «Blau» és per a dibuixar la línia "
"artística auxiliar en blau, el qual s'utilitza per especificar les ombres i "
"la capa «Ombra» és per a dibuixar la línia artística auxiliar en verd clar, "
"el qual s'utilitza per especificar ombres més fosques. No obstant això, "
"probablement hàgiu d'augmentar o disminuir aquestes capes d'acord amb el "
"vostre treball."

#: ../../user_manual/japanese_animation_template.rst:75
msgid ".. image:: images/en/Time_sheet_1.png"
msgstr ".. image:: images/en/Time_sheet_1.png"

#: ../../user_manual/japanese_animation_template.rst:76
msgid ""
"Finished keys, you will begin to draw the inbetweens. If you feel Krita is "
"becoming slow, I recommend you to merge key drafts and keys, as well as to "
"remove any unnecessary layers."
msgstr ""
"En finalitzar les claus, començareu a dibuixar els entremigs. Si trobeu que "
"el Krita s'està tornant lent, recomano fusionar els esbossos i les claus, "
"així com eliminar qualsevol capa innecessària."

#: ../../user_manual/japanese_animation_template.rst:78
msgid ""
"After finalizing keys and cleaning up unnecessary layers, add inbetweenings, "
"using Time sheet and inbetweening drafts as reference."
msgstr ""
"Després de finalitzar les claus i netejar les capes innecessàries, afegiu "
"els entremigs, utilitzant el full de temps i els esbossos d'entremig com a "
"referència."

#: ../../user_manual/japanese_animation_template.rst:81
msgid "This is its correspondence with Time sheet."
msgstr "Aquesta és la seva correspondència amb el full de temps."

#: ../../user_manual/japanese_animation_template.rst:84
msgid ".. image:: images/en/Inbetweening.png"
msgstr ".. image:: images/en/Inbetweening.png"

#: ../../user_manual/japanese_animation_template.rst:85
msgid ""
"Once the vector functionality of Krita becomes better, I recommend you to "
"use vector to finalize inbetweening."
msgstr ""
"Una vegada que la funcionalitat vectorial del Krita es torni millor, "
"recomano que utilitzeu vectors per a finalitzar l'entremig."

#: ../../user_manual/japanese_animation_template.rst:87
msgid ""
"If you do the colors in Krita, please use Coloring group layer. If you do "
"colors in other software, I recommend to export frames as .TGA files."
msgstr ""
"Si feu els colors en el Krita, empreu la capa de grup per a donar color. Si "
"feu els colors en un altre programari, recomano exportar els marcs com a "
"fitxers «.TGA»."

#: ../../user_manual/japanese_animation_template.rst:91
msgid "Resolution"
msgstr "Resolució"

#: ../../user_manual/japanese_animation_template.rst:93
msgid ""
"I made this template in 300 dpi because we have to print them to use them in "
"traditional works which still fill an important role in Japanese Anime "
"Studio. However, if you stick to digital, 150-120 dpi is enough to make "
"animation. So you can decrease its resolution according to your need."
msgstr ""
"Vaig fer aquesta plantilla en 300 ppp (dpi) perquè haurem d'imprimir-la per "
"a utilitzar-la en treballs tradicionals que encara tenen un paper important "
"a l'estudi d'anime japonès. No obstant això, si ens atenim al digital, de "
"150 a 120 ppp (dpi) n'hi ha prou per a crear animació. Així podreu disminuir "
"la seva resolució d'acord amb les vostres necessitats."

#: ../../user_manual/japanese_animation_template.rst:95
msgid ""
"Originally written by Saisho Kazuki, Japanese professional animator, and "
"translated by Tokiedian, KDE contributor."
msgstr ""
"Originalment escrit per en Saisho Kazuki, animador professional japonès i "
"traduït per en Tokiedian, col·laborador del KDE."
