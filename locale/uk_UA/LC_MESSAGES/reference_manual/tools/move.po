# Translation of docs_krita_org_reference_manual___tools___move.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___move\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-25 03:36+0100\n"
"PO-Revision-Date: 2019-03-12 16:54+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Position"
msgstr "Розташування"

#: ../../<rst_epilog>:44
msgid ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: toolmove"
msgstr ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: toolmove"

#: ../../reference_manual/tools/move.rst:0
msgid ".. image:: images/en/Movetool_coordinates.png"
msgstr ".. image:: images/en/Movetool_coordinates.png"

#: ../../reference_manual/tools/move.rst:1
msgid "Krita's move tool reference."
msgstr "Довідник із інструмента пересування Krita."

#: ../../reference_manual/tools/move.rst:16
msgid "Move Tool"
msgstr "Інструмент «Пересування»"

#: ../../reference_manual/tools/move.rst:18
msgid "|toolmove|"
msgstr "|toolmove|"

#: ../../reference_manual/tools/move.rst:20
msgid ""
"With this tool, you can move the current layer or selection by dragging the "
"mouse."
msgstr ""
"За допомогою цього інструмента ви можете пересунути поточний шар або "
"позначену область перетягуванням вказівником миші."

#: ../../reference_manual/tools/move.rst:22
msgid "Move current layer"
msgstr "Пересунути поточний шар"

#: ../../reference_manual/tools/move.rst:23
msgid "Anything that is on the selected layer will be moved"
msgstr "Буде пересунуто усе на позначеному шарі."

#: ../../reference_manual/tools/move.rst:24
msgid "Move layer with content"
msgstr "Пересунути шар з вмістом"

#: ../../reference_manual/tools/move.rst:25
msgid ""
"Any content contained on the layer that is resting under the four-headed "
"Move cursor will be moved"
msgstr ""
"Буде пересунуто усе, що міститься на шарі, який розташовано під "
"чотиристрілковим вказівником для пересування."

#: ../../reference_manual/tools/move.rst:26
msgid "Move the whole group"
msgstr "Пересунути цілу групу"

#: ../../reference_manual/tools/move.rst:27
msgid ""
"All content on all layers will move.  Depending on the number of layers this "
"might result in slow and, sometimes, jerky movements. Use this option "
"sparingly or only when necessary."
msgstr ""
"Буде пересунуто усе на усіх шарах. Залежно від кількості шарів, це може "
"призвести до уповільнення та, іноді, пересування ривками. Користуйтеся цим "
"варіантом обережно або лише за потреби."

#: ../../reference_manual/tools/move.rst:28
msgid "Shortcut move distance (3.0+)"
msgstr "Відстань пересування після натискання (3.0+)"

#: ../../reference_manual/tools/move.rst:29
msgid ""
"This allows you to set how much, and in which units, the :kbd:`&larr;`, :kbd:"
"`&uarr;`, :kbd:`&rarr;` and :kbd:`&darr;` actions will move the layer."
msgstr ""
"За допомогою цього пункту ви можете вказати на скільки і у яких одиницях "
"стрілки :kbd:`&larr;`, :kbd:`&uarr;`, :kbd:`&rarr;` і :kbd:`&darr;` "
"пересуватимуть шар."

#: ../../reference_manual/tools/move.rst:30
msgid "Large Move Scale (3.0+)"
msgstr "Велика зміна масштабу (3.0+)"

#: ../../reference_manual/tools/move.rst:31
msgid ""
"Allows you to multiply the movement of the Shortcut Move Distance when "
"pressing :kbd:`Shift` before pressing a direction key."
msgstr ""
"Надає вам змогу помножити відстань пересування за клавіатурним скороченням, "
"якщо перед натисканням клавіші напрямку натиснуто клавішу :kbd:`Shift`."

#: ../../reference_manual/tools/move.rst:32
msgid "Show coordinates"
msgstr "Показувати координати"

#: ../../reference_manual/tools/move.rst:33
msgid ""
"When toggled will show the coordinates of the top-left pixel of the moved "
"layer in a floating window."
msgstr ""
"Якщо позначено, програма показуватиме координати верхнього лівого пікселя "
"пересунутого шару у рухомому вікні."

#: ../../reference_manual/tools/move.rst:35
msgid ""
"If you click, then press :kbd:`Shift`, then move the layer, movement is "
"constrained to the horizontal and vertical directions. If you press :kbd:"
"`Shift`, then click, then move, all layers will be moved, with the movement "
"constrained to the horizontal and vertical directions"
msgstr ""
"Якщо ви натиснете ліву кнопку миші, потім натиснете клавішу :kbd:`Shift`, а "
"далі пересунете шар, рух буде обмежено горизонтальним або вертикальним "
"напрямком. Якщо ви натиснете клавішу :kbd:`Shift`, натиснете ліву кнопку "
"миші, потім пересунете вказівник, буде пересунуто усі шари із обмеженням "
"руху горизонтальним або вертикальним напрямком."

#: ../../reference_manual/tools/move.rst:37
msgid "Constrained movement"
msgstr "Обмежене пересування"

#: ../../reference_manual/tools/move.rst:40
msgid ""
"Gives the top-left coordinate of the layer, can also be manually edited."
msgstr "Показує верхню ліву координату шару, також можна редагувати вручну."
