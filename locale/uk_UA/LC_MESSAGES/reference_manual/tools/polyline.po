# Translation of docs_krita_org_reference_manual___tools___polyline.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___polyline\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-04-04 15:42+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:32
msgid ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"
msgstr ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"

#: ../../reference_manual/tools/polyline.rst:1
msgid "Krita's polyline tool reference."
msgstr "Довідник із інструмента ламаних Krita."

#: ../../reference_manual/tools/polyline.rst:15
msgid "Polyline Tool"
msgstr "Інструмент «Ламана лінія»"

#: ../../reference_manual/tools/polyline.rst:17
msgid "|toolpolyline|"
msgstr "|toolpolyline|"

#: ../../reference_manual/tools/polyline.rst:20
msgid ""
"Polylines are drawn like :ref:`polygon_tool`, with the difference that the "
"double-click indicating the end of the polyline does not connect the last "
"vertex to the first one."
msgstr ""
"Ламані лінії малюють так, як і за допомогою :ref:`polygon_tool`. Відмінність "
"полягає лише у тому, що після подвійного клацання, яке вказує на завершення "
"малювання ламаної, програма не з'єднує початкову вершину із останньою."
