# Translation of docs_krita_org_reference_manual___filters___adjust.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___adjust\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-17 03:39+0200\n"
"PO-Revision-Date: 2019-04-17 07:30+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Power"
msgstr "Потужність"

#: ../../reference_manual/filters/adjust.rst:None
msgid ".. image:: images/en/Krita_filters_asc_cdl.png"
msgstr ".. image:: images/en/Krita_filters_asc_cdl.png"

#: ../../reference_manual/filters/adjust.rst:1
msgid "Overview of the adjust filters."
msgstr "Огляд фільтрів коригування."

#: ../../reference_manual/filters/adjust.rst:16
msgid "Adjust"
msgstr "Коригування"

#: ../../reference_manual/filters/adjust.rst:18
msgid ""
"The Adjustment filters are image-wide and are for manipulating colors and "
"contrast."
msgstr ""
"Фільтри коригування застосовуються до усього зображення. Їх призначено для "
"керування кольорами і контрастністю."

#: ../../reference_manual/filters/adjust.rst:23
msgid "Dodge"
msgstr "Висвітлювання"

#: ../../reference_manual/filters/adjust.rst:25
msgid ""
"An image-wide dodge-filter. Dodge is named after a trick in traditional dark-"
"room photography that gave the same results."
msgstr ""
"Фільтр висвітлення для усього зображення. Назва походить від подібного трюку "
"у традиційному фотомистецтві."

#: ../../reference_manual/filters/adjust.rst:28
msgid ".. image:: images/en/Dodge-filter.png"
msgstr ".. image:: images/en/Dodge-filter.png"

#: ../../reference_manual/filters/adjust.rst:29
#: ../../reference_manual/filters/adjust.rst:47
msgid "Shadows"
msgstr "Тіні"

#: ../../reference_manual/filters/adjust.rst:30
#: ../../reference_manual/filters/adjust.rst:48
msgid "The effect will mostly apply to dark tones."
msgstr "Ефект здебільшого застосовується до темних тонів."

#: ../../reference_manual/filters/adjust.rst:31
#: ../../reference_manual/filters/adjust.rst:49
msgid "Midtones"
msgstr "Півтони"

#: ../../reference_manual/filters/adjust.rst:32
#: ../../reference_manual/filters/adjust.rst:50
msgid "The effect will apply to mostly midtones."
msgstr "Ефекти здебільшого застосовується до помірних тонів."

#: ../../reference_manual/filters/adjust.rst:33
#: ../../reference_manual/filters/adjust.rst:51
msgid "Highlights"
msgstr "Виблиски"

#: ../../reference_manual/filters/adjust.rst:34
#: ../../reference_manual/filters/adjust.rst:52
msgid "This will apply the effect on the highlights only."
msgstr "Вибір цього пункту призведе до застосування ефекту лише для виблисків."

#: ../../reference_manual/filters/adjust.rst:36
#: ../../reference_manual/filters/adjust.rst:54
msgid "Exposure"
msgstr "Експозиція"

#: ../../reference_manual/filters/adjust.rst:36
#: ../../reference_manual/filters/adjust.rst:54
msgid "The strength at which this filter is applied."
msgstr "Потужність застосування фільтра."

#: ../../reference_manual/filters/adjust.rst:41
msgid "Burn"
msgstr "Вигорання"

#: ../../reference_manual/filters/adjust.rst:43
msgid ""
"An image-wide burn-filter. Burn is named after a trick in traditional dark-"
"room photography that gave similar results."
msgstr ""
"Фільтр вигоряння для усього зображення. Назва походить від подібного трюку у "
"традиційному фотомистецтві."

#: ../../reference_manual/filters/adjust.rst:46
msgid ".. image:: images/en/Burn-filter.png"
msgstr ".. image:: images/en/Burn-filter.png"

#: ../../reference_manual/filters/adjust.rst:59
msgid "Levels"
msgstr "Рівні"

#: ../../reference_manual/filters/adjust.rst:61
msgid ""
"This filter allows you to directly modify the levels of the tone-values of "
"an image, by manipulating sliders for highlights, midtones and shadows. You "
"can even set an output and input range of tones for the image. A histogram "
"is displayed to show you the tonal distribution. The default shortcut for "
"levels filter is :kbd:`Ctrl + L` ."
msgstr ""
"За допомогою цього фільтра ви можете безпосередньо вносити зміни до рівнів "
"значень тонів зображення, пересуваючи повзунки для виблисків, середніх тонів "
"та тіней. Ви навіть можете встановити вихідний і вхідний діапазони тонів для "
"зображення. Програма показуватиме гістограму для контролю за розподілом "
"тонів. Типовим клавіатурним скороченням для фільтра рівнів є :kbd:`Ctrl + "
"L` ."

#: ../../reference_manual/filters/adjust.rst:65
msgid ".. image:: images/en/Levels-filter.png"
msgstr ".. image:: images/en/Levels-filter.png"

#: ../../reference_manual/filters/adjust.rst:66
msgid ""
"This is very useful to do an initial cleanup of scanned lineart or grayscale "
"images. If the scanned lineart is light you can slide the black triangle to "
"right to make it darker or if you want to remove the gray areas you can "
"slide the white slider to left."
msgstr ""
"Це дуже корисно для початкового чищення сканованої графіки або зображень у "
"відтінках сірого. Якщо сканована графіка є світлою, ви можете перетягнути "
"чорний трикутник праворуч, щоб зробити графіку темнішою, або, якщо хочете "
"вилучити сірі ділянки, ви можете перетягнути білий повзунок ліворуч."

#: ../../reference_manual/filters/adjust.rst:68
msgid ""
"Auto levels is a quick way to adjust tone of an image. If you want to change "
"the settings later you can click on the :guilabel:`Create Filter Mask` "
"button to add the levels as a filter mask."
msgstr ""
"Авторівні — швидкий спосіб коригування тонів вашого зображення. Якщо вам "
"потрібно змінити параметри пізніше, ви можете натиснути кнопку :guilabel:"
"`Створити маску фільтрування` для додавання рівнів як масок фільтрування."

#: ../../reference_manual/filters/adjust.rst:74
msgid "Color Adjustment Curves"
msgstr "Криві коригування кольорів"

#: ../../reference_manual/filters/adjust.rst:76
msgid ""
"This filter allows you to adjust each channel by manipulating the curves. "
"You can even adjust the alpha channel and the lightness channel through this "
"filter. This is used very often by artists as a post processing filter to "
"slightly heighten the mood of the painting by adjust the overall color. For "
"example a scene with fire breathing dragon may be made more red and yellow "
"by adjusting the curves to give it more warmer look, similarly a snowy "
"mountain scene can be made to look cooler by adjusting the blues and greens. "
"The default shortcut for this filter is :kbd:`Ctrl + M`."
msgstr ""
"За допомогою цього фільтра можна коригувати усі канали, працюючи з кривими. "
"За допомогою цього фільтра ви навіть можете скоригувати альфа-канал та канал "
"освітленості. Цей канал часто використовують для остаточної обробки "
"фільтрами і коригування настрою зображення шляхом виправлення загального "
"кольору. Наприклад, сцену з драконом, який дихає полум'ям, можна зробити "
"червонішою і жовтішою, скоригувавши криві з метою надання їй гарячішого "
"вигляду. Так само, сцену зі сніговими горами можна зробити холоднішою, "
"скоригувавши сині і зелені тони. Типовим клавіатурним скороченням для цього "
"фільтра є комбінація клавіш :kbd:`Ctrl + M`."

#: ../../reference_manual/filters/adjust.rst:81
msgid "Since 4.1 this filter can also handle Hue and Saturation curves."
msgstr ""
"Починаючи з версії 4.1 цей фільтр можна застосовувати і до кривих відтінку і "
"насиченості."

#: ../../reference_manual/filters/adjust.rst:84
msgid ".. image:: images/en/Color-adjustment-curve.png"
msgstr ".. image:: images/en/Color-adjustment-curve.png"

#: ../../reference_manual/filters/adjust.rst:88
msgid "Cross-channel color adjustment"
msgstr "Міжканальне коригування кольорів"

#: ../../reference_manual/filters/adjust.rst:92
msgid ""
"Sometimes, when you are adjusting the colors for an image, you want bright "
"colors to be more saturated, or have a little bit of brightness in the "
"purples."
msgstr ""
"Іноді, коли ви коригуєте кольори зображення, бажаним є більше насичення "
"яскравих кольорів або додавання більшої яскравості до пурпурових кольорів."

#: ../../reference_manual/filters/adjust.rst:94
msgid "The Cross-channel color adjustment filter allows you to do this."
msgstr ""
"Досягти потрібного результату можна за допомогою міжканального фільра "
"коригування кольорів."

#: ../../reference_manual/filters/adjust.rst:96
msgid ""
"At the top, there are two drop-downs. The first one is to choose which :"
"guilabel:`Channel` you wish to modify. The :guilabel:`Driver Channel` drop "
"down is what channel you use to control which parts are modified."
msgstr ""
"У верхній частині вікна розташовано два спадних списки. Перший з них "
"призначено для вибору :guilabel:`Каналу` для внесення змін. За допомогою "
"спадного списку :guilabel:`Основний канал` ви можете вибрати канал, який "
"буде використано для керування тим, до яких частин вносяться зміни."

#: ../../reference_manual/filters/adjust.rst:99
msgid ".. image:: images/en/cross_channel_filter.png"
msgstr ".. image:: images/en/cross_channel_filter.png"

#: ../../reference_manual/filters/adjust.rst:100
msgid ""
"The curve, on the horizontal axis, represents the driver channel, while the "
"vertical axis represent the channel you wish to modify."
msgstr ""
"За горизонтальною віссю на полі кривої відкладено дані каналу драйвера, а за "
"вертикальною — канал, до якого ви хочете внести зміни."

#: ../../reference_manual/filters/adjust.rst:102
msgid ""
"So if you wish to increase the saturation in the lighter parts, you pick :"
"guilabel:`Saturation` in the first drop-down, and :guilabel:`Lightness` as "
"the driver channel. Then, pull up the right end to the top."
msgstr ""
"Якщо ви хочете збільшити насиченість світліших частин, виберіть :guilabel:"
"`Насиченість` з першого спадного списку і :guilabel:`Освітленість` у списку "
"основного каналу. Далі перетягніть правий кінець вгору."

#: ../../reference_manual/filters/adjust.rst:104
msgid ""
"If you wish to desaturate everything but the teal/blues, you select :"
"guilabel:`Saturation` for the channel and :guilabel:`Hue` for the driver. "
"Then put a dot in the middle and pull down the dots on either sides."
msgstr ""
"Якщо ви хочете зменшити насиченість усього, окрім синьо-зелених тонів, "
"виберіть :guilabel:`Насиченість` для каналу і :guilabel:`Відтінок` для "
"основного каналу. Далі, додайте точку посередині і перетягніть вниз точки на "
"обох боках."

#: ../../reference_manual/filters/adjust.rst:107
msgid "Brightness/Contrast curves"
msgstr "Криві яскравості/контрастності"

#: ../../reference_manual/filters/adjust.rst:109
msgid ""
"This filter allows you to adjust the brightness and contrast of the image by "
"adjusting the curves."
msgstr ""
"За допомогою цього фільтра ви зможете скоригувати яскравість і контрастність "
"зображення за допомогою коригування кривих."

#: ../../reference_manual/filters/adjust.rst:113
msgid ""
"These have been removed in Krita 4.0, because the Color Adjustment filter "
"can do the same. Old files with brightness/contrast curves will be loaded as "
"Color Adjustment curves"
msgstr ""
"Ці криві було вилучено у Krita 4.0, оскільки ці дії можна виконувати за "
"допомогою фільтра коригування кольорів. Застарілі файли із кривими "
"яскравості-контрастності буде завантажено як файли із кривими користування "
"кольорів."

#: ../../reference_manual/filters/adjust.rst:118
msgid "Color Balance"
msgstr "Баланс кольорів"

#: ../../reference_manual/filters/adjust.rst:120
msgid ""
"This filter allows you to control the color balance of the image by "
"adjusting the sliders for Shadows, Midtones and Highlights. The default "
"shortcut for this filter is :kbd:`Ctrl + B` ."
msgstr ""
"За допомогою цього фільтра ви можете керувати балансом кольорів зображення: "
"достатньо пересунути відповідним чином повзунки для тіней, середніх тонів та "
"виблисків. Типовим клавіатурним скороченням для цього фільтра є :kbd:`Ctrl + "
"B`."

#: ../../reference_manual/filters/adjust.rst:123
msgid ".. image:: images/en/Color-balance.png"
msgstr ".. image:: images/en/Color-balance.png"

#: ../../reference_manual/filters/adjust.rst:127
msgid "Desaturate"
msgstr "Прибрати насичення"

#: ../../reference_manual/filters/adjust.rst:129
msgid ""
"Image-wide desaturation filter. Will make any image Grayscale. Has several "
"choices by which logic the colors are turned to gray. The default shortcut "
"for this filter is :kbd:`Ctrl + Shift + U` ."
msgstr ""
"Загальний фільтр зменшення насиченості для усього зображення. Перетворює "
"будь-яке зображення на зображення у відтінках сірого. Має декілька варіантів "
"логіки перетворення кольорів на відтінки сірого. Типовим клавіатурним "
"скороченням для цього фільтра є :kbd:`Ctrl + Shift + U` ."

#: ../../reference_manual/filters/adjust.rst:133
msgid ".. image:: images/en/Desaturate-filter.png"
msgstr ".. image:: images/en/Desaturate-filter.png"

#: ../../reference_manual/filters/adjust.rst:134
msgid "Lightness"
msgstr "Освітленість"

#: ../../reference_manual/filters/adjust.rst:135
msgid "This will turn colors to gray using the HSL model."
msgstr "Перетворює кольори на відтінки сірого кольору на основі моделі HSL."

#: ../../reference_manual/filters/adjust.rst:136
msgid "Luminosity (ITU-R BT.709)"
msgstr "Світність (ITU-R BT.709)"

#: ../../reference_manual/filters/adjust.rst:137
msgid ""
"Will turn the color to gray by using the appropriate amount of weighting per "
"channel according to ITU-R BT.709."
msgstr ""
"Виконує перетворення кольорів на відтінки сірого на основі відповідної суми "
"зважених значень каналів відповідно до ITU-R BT.709."

#: ../../reference_manual/filters/adjust.rst:138
msgid "Luminosity (ITU-R BT.601)"
msgstr "Світність (ITU-R BT.601)"

#: ../../reference_manual/filters/adjust.rst:139
msgid ""
"Will turn the color to gray by using the appropriate amount of weighting per "
"channel according to ITU-R BT.601."
msgstr ""
"Виконує перетворення кольорів на відтінки сірого на основі відповідної суми "
"зважених значень каналів відповідно до ITU-R BT.601."

#: ../../reference_manual/filters/adjust.rst:140
msgid "Average"
msgstr "Середнє"

#: ../../reference_manual/filters/adjust.rst:141
msgid "Will make an average of all channels."
msgstr "Усереднює усі канали."

#: ../../reference_manual/filters/adjust.rst:142
msgid "Min"
msgstr "Мін."

#: ../../reference_manual/filters/adjust.rst:143
msgid "Subtracts all from one another to find the gray value."
msgstr "Віднімає усі значення для пошуку значення сірого."

#: ../../reference_manual/filters/adjust.rst:145
msgid "Max"
msgstr "Макс."

#: ../../reference_manual/filters/adjust.rst:145
msgid "Adds all channels together to get a gray value"
msgstr "Додає усі канали для отримання значення сірого."

#: ../../reference_manual/filters/adjust.rst:150
msgid "Invert"
msgstr "Інвертувати"

#: ../../reference_manual/filters/adjust.rst:152
msgid ""
"This filter like the name suggests inverts the color values in the image. So "
"white (1,1,1) becomes black (0,0,0), yellow (1,1,0) becomes blue (0,1,1), "
"etc. The default shortcut for this filter is :kbd:`Ctrl + I`."
msgstr ""
"Цей фільтр, як можна зрозуміти з назви, інвертує значення кольорів "
"зображення. Отже, білий (1,1,1) стане чорним (0,0,0), жовтий (1,1,0) стане "
"синім (0,1,1), тощо. Типовим клавіатурним скороченням для цього фільтра є :"
"kbd:`Ctrl + I`."

#: ../../reference_manual/filters/adjust.rst:158
msgid "Auto Contrast"
msgstr "Автоконтрастність"

#: ../../reference_manual/filters/adjust.rst:160
msgid "Tries to adjust the contrast the universally acceptable levels."
msgstr ""
"Намагається скоригувати контрастність до універсальних прийнятних значень."

#: ../../reference_manual/filters/adjust.rst:165
msgid "HSV/HSL Adjustment"
msgstr "Коригування HSV/HSL"

#: ../../reference_manual/filters/adjust.rst:167
msgid ""
"With this filter, you can adjust the Hue, Saturation, Value or Lightness, "
"through sliders. The default shortcut for this filter is :kbd:`Ctrl + U` ."
msgstr ""
"За допомогою цього фільтра ви можете скоригувати відтінок, насиченість і "
"значення або освітленість, пересуваючи відповідні повзунки. Типовим "
"клавіатурним скороченням для фільтра є комбінація клавіш :kbd:`Ctrl + U` ."

#: ../../reference_manual/filters/adjust.rst:170
msgid ".. image:: images/en/Hue-saturation-filter.png"
msgstr ".. image:: images/en/Hue-saturation-filter.png"

#: ../../reference_manual/filters/adjust.rst:174
msgid "Threshold"
msgstr "Поріг"

#: ../../reference_manual/filters/adjust.rst:176
msgid ""
"A simple black and white threshold filter that uses sRGB luminosity. It'll "
"convert any image to a image with only black and white, with the input "
"number indicating the threshold value at which black becomes white."
msgstr ""
"Прости чорно-білий пороговий фільтр, який використовує значення світності "
"sRGB. Перетворює будь-яке зображення на чорно-біле, вхідне число визначає "
"порогове значення, яке розмежовує області чорного і білого кольорів."

#: ../../reference_manual/filters/adjust.rst:181
msgid "Slope, Offset, Power"
msgstr "Нахил, зсув, сила"

#: ../../reference_manual/filters/adjust.rst:183
msgid ""
"A different kind of color balance filter, with three color selectors, which "
"will have the same shape as the one used in settings."
msgstr ""
"Інший тип фільтра балансування кольорів із трьома засобами вибору кольору, "
"який матиме ту саму форму, як і та, яку використано у параметрах."

#: ../../reference_manual/filters/adjust.rst:185
msgid ""
"This filter is particular useful because it has been defined by the American "
"Society for Cinema as \"ASC_CDL\", meaning that it is a standard way of "
"describing a color balance method."
msgstr ""
"Цей фільтр є корисним, оскільки його визначено Американською спілкою кіно як "
"«ASC_CDL», тобто це стандартний спосіб опису методу балансування кольорів."

#: ../../reference_manual/filters/adjust.rst:191
msgid "Slope"
msgstr "Нахил"

#: ../../reference_manual/filters/adjust.rst:192
msgid ""
"This represents a multiplication and determine the adjustment of the "
"brighter colors in an image."
msgstr ""
"Цей пункт відповідає множенню. Його призначено для визначення коригування "
"світліших кольорів на зображенні."

#: ../../reference_manual/filters/adjust.rst:193
msgid "Offset"
msgstr "Зміщення"

#: ../../reference_manual/filters/adjust.rst:194
msgid ""
"This determines how much the bottom is offset from the top, and so "
"determines the color of the darkest colors."
msgstr ""
"Визначає наскільки великим є відступ нижньої частини від верхньої, отже "
"визначає колір найтемніших кольорів."

#: ../../reference_manual/filters/adjust.rst:196
msgid ""
"This represents a power function, and determines the adjustment of the mid-"
"tone to dark colors of an image."
msgstr ""
"Цей пункт відповідає степеневій функції. Його призначено для коригування "
"діапазону кольорів на зображенні від середніх до темних."
