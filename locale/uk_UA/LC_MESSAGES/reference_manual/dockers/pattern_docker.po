# Translation of docs_krita_org_reference_manual___dockers___pattern_docker.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___pattern_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-03-19 08:21+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/pattern_docker.rst:1
msgid "Overview of the pattern docker."
msgstr "Огляд бічної панелі візерунків."

#: ../../reference_manual/dockers/pattern_docker.rst:16
msgid "Patterns Docker"
msgstr "Бічна панель візерунків"

#: ../../reference_manual/dockers/pattern_docker.rst:19
msgid ".. image:: images/en/Krita_Patterns_Docker.png"
msgstr ".. image:: images/en/Krita_Patterns_Docker.png"

#: ../../reference_manual/dockers/pattern_docker.rst:20
msgid ""
"This docker allows you to select the global pattern. Using the open-file "
"button you can import patterns. Some common shortcuts are the following:"
msgstr ""
"За допомогою цієї бічної панелі ви можете вибрати загальний візерунок. Для "
"імпортування візерунків скористайтеся кнопкою відкриття файла. Ось деякі "
"загальні скорочення для керування:"

#: ../../reference_manual/dockers/pattern_docker.rst:22
msgid "|mouseright| a swatch will allow you to set tags."
msgstr ""
"Клацання |mouseright| на пункті набору надасть вам змогу встановити мітки."

#: ../../reference_manual/dockers/pattern_docker.rst:23
msgid "|mouseleft| a swatch will allow you to set it as global pattern."
msgstr ""
"|mouseleft| на пункті набору надасть вам змогу встановити його як загальний "
"візерунок."

#: ../../reference_manual/dockers/pattern_docker.rst:24
msgid ":kbd:`Ctrl` + Scroll you can resize the swatch sizes."
msgstr ""
":kbd:`Ctrl` + прокручування коліщатка надає змогу змінювати розміри пунктів "
"набору."
