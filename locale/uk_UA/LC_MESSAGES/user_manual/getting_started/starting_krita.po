# Translation of docs_krita_org_user_manual___getting_started___starting_krita.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_user_manual___getting_started___starting_krita\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-25 03:36+0100\n"
"PO-Revision-Date: 2019-02-25 21:52+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"

#: ../../user_manual/getting_started/starting_krita.rst:None
msgid ".. image:: images/en/Starting-krita.png"
msgstr ".. image:: images/en/Starting-krita.png"

#: ../../user_manual/getting_started/starting_krita.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"Прости підручник із перших кроків користування Krita: створення і збереження "
"зображення."

#: ../../user_manual/getting_started/starting_krita.rst:22
msgid "Starting Krita"
msgstr "Запуск Krita"

#: ../../user_manual/getting_started/starting_krita.rst:24
msgid ""
"There will be no canvas or new document open by default. To create a new "
"canvas you have to create a new document from the :guilabel:`File` menu. If "
"you want to open an existing image, either use :menuselection:`File --> "
"Open` or drag the image from your computer into Krita's window."
msgstr ""
"Типово, після першого запуску не буде показано полотна чи нового документа. "
"Щоб створити полотно, вам слід створити документ за допомогою меню :guilabel:"
"`Файл`. Якщо ви хочете відкрити наявне зображення, скористайтеся або "
"пунктом :menuselection:`Файл --> Відкрити`, або перетягніть пункт зображення "
"із програми для керування файлами вашого комп'ютера і скиньте його до вікна "
"Krita."

#: ../../user_manual/getting_started/starting_krita.rst:30
msgid "Creating a New Document"
msgstr "Створення документа"

#: ../../user_manual/getting_started/starting_krita.rst:32
msgid "A new document can be created as follows."
msgstr "Нижче наведено настанови зі створення документа."

#: ../../user_manual/getting_started/starting_krita.rst:34
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr ""
"Натисніть пункт :guilabel:`Файл` у меню програми, розташованому у верхній "
"частині вікна."

#: ../../user_manual/getting_started/starting_krita.rst:35
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing :kbd:`Ctrl + "
"N`."
msgstr ""
"Далі, натисніть пункт :guilabel:`Створити`. Ви також можете скористатися "
"натисканням комбінації клавіш :kbd:`Ctrl + N`."

#: ../../user_manual/getting_started/starting_krita.rst:36
msgid "Now you will get a New Document dialog box as shown below:"
msgstr ""
"У відповідь ви маєте побачити діалогове вікно «Створення документа», яке "
"показано нижче:"

#: ../../user_manual/getting_started/starting_krita.rst:39
msgid ".. image:: images/en/Krita_newfile.png"
msgstr ".. image:: images/en/Krita_newfile.png"

#: ../../user_manual/getting_started/starting_krita.rst:40
msgid ""
"Krita is a pretty complex program that can handle a lot of different files, "
"so let's go through this step by step:"
msgstr ""
"Krita є доволі складною програмою, яка може працювати із багатьма різними "
"файлами, тому розберемо усе крок за кроком:"

#: ../../user_manual/getting_started/starting_krita.rst:43
msgid "Custom Document"
msgstr "Нетиповий документ"

#: ../../user_manual/getting_started/starting_krita.rst:45
msgid ""
"From this section you can create a document according to your requirements: "
"you can specify the dimensions, color model, depth, resolution, etc."
msgstr ""
"За допомогою цього розділу ви зможете створити документ за визначеними вами "
"параметрами: ви можете вказати розмірність зображення, модель кольорів, "
"глибину кольорів, роздільну здатність тощо."

#: ../../user_manual/getting_started/starting_krita.rst:47
msgid ""
"In the top-most field of the :guilabel:`Dimensions` tab, you can define a "
"name for your new document. This name will appear in the metadata of the "
"file, and Krita will use it for the auto-save functionality as well. If you "
"leave it empty, the document will be referred to as 'Unnamed' by default."
msgstr ""
"За допомогою верхнього поля на вкладці :guilabel:`Розміри` ви можете "
"визначити назву вашого нового документа. Цю назву буде збережено у метаданих "
"файла; крім того, Krita використовуватиме її для виконання дій із "
"автоматичного збереження даних. Якщо ви не вказуватимете назву, буде "
"використано типову назву — «Без назви»."

#: ../../user_manual/getting_started/starting_krita.rst:49
msgid ""
"From the Predefined drop-down you can select predefined pixel sizes and PPI "
"(pixels per inch)."
msgstr ""
"За допомогою спадного списку стандартних значень ви можете вибрати "
"попередньо визначені розміри у пікселях та щільність пікселів на дюйм (PPI)."

#: ../../user_manual/getting_started/starting_krita.rst:51
msgid ""
"You can set custom dimensions and the orientation of the document from the "
"input fields below the predefined drop-down. This can also be saved as a new "
"predefined preset for your future use by giving a name in the Save As field "
"and clicking on the Save button."
msgstr ""
"Ви можете встановити нетипові розміри і орієнтацію документа за допомогою "
"полів введення даних під спадним списком «Стандартний». Ви також можете "
"зберегти вказані значення як новий стандартний набір налаштувань для "
"майбутнього використання, надавши йому назву у полі «Зберегти як» і "
"натиснувши кнопку «Зберегти»."

#: ../../user_manual/getting_started/starting_krita.rst:53
msgid ""
"Below we find the Color section of the new document dialog box, where you "
"can select the color model and the bit-depth. Check :ref:"
"`color_management_settings` for more info."
msgstr ""
"Нижче розташовано розділ «Колір» діалогового вікна створення документа. У "
"цьому розділі ви можете вибрати модель кольорів і глибину кольорів. Щоб "
"дізнатися більше про кольори, ознайомтеся із розділом :ref:"
"`color_management_settings`."

#: ../../user_manual/getting_started/starting_krita.rst:55
msgid ""
"On the :guilabel:`Content` tab, you can select the background color and the "
"amount of layers you want in the new document. Krita remembers the amount of "
"layers you picked last time, so be careful."
msgstr ""
"На вкладці :guilabel:`Вміст` ви можете вибрати колір тла і кількість шарів у "
"новому документі. Krita запам'ятовує останню вказану кількість шарів, отже, "
"будьте обережні."

#: ../../user_manual/getting_started/starting_krita.rst:57
msgid ""
"Finally, there's a description box, useful to note down what you are going "
"to do."
msgstr ""
"Нарешті, передбачено поле опису, яким можна скористатися для додавання опису "
"ваших намірів щодо зображення."

#: ../../user_manual/getting_started/starting_krita.rst:60
msgid "Create From Clipboard"
msgstr "Створити на основі вмісту буфера обміну"

#: ../../user_manual/getting_started/starting_krita.rst:62
msgid ""
"This section allows you to create a document from an image that is in your "
"clipboard, like a screenshot. It will have all the fields set to match the "
"clipboard image."
msgstr ""
"За допомогою цього розділу ви можете створити документ на основі даних "
"зображення, які було збережено до буфера обміну даними, як на нашому знімку "
"вікна. Усі параметри новоствореного зображення буде визначено за даними з "
"буфера обміну."

#: ../../user_manual/getting_started/starting_krita.rst:65
msgid "Templates:"
msgstr "Шаблони:"

#: ../../user_manual/getting_started/starting_krita.rst:67
msgid ""
"These are separate categories where we deliver special defaults. Templates "
"are just .kra files which are saved in a special location, so they can be "
"pulled up by Krita quickly."
msgstr ""
"Це окремі категорії, де ви можете вказати особливі типові значення. Шаблони "
"— це просто файли .kra, які збережено у особливому місці, так, щоб Krita "
"могла без проблем отримувати ці дані."

#: ../../user_manual/getting_started/starting_krita.rst:69
msgid ""
"You can make your own template file from any .kra file, by using :"
"menuselection:`File --> Create Template From Image` in the top menu. This "
"will add your current document as a new template, including all its "
"properties along with the layers and layer contents."
msgstr ""
"Ви можете створити ваш власний файл шаблона на основі будь-якого файла .kra "
"за допомогою пункту меню :menuselection:`Файл --> Створити шаблон з "
"зображення` основного меню програми. Ваш поточний документ буде додано як "
"новий шаблон, включаючи усі його властивості, разом із шарами та вмістом "
"шарів."

#: ../../user_manual/getting_started/starting_krita.rst:71
msgid ""
"Once you have created a new document according to your preference, you "
"should now have a white canvas in front of you (or whichever background "
"color you chose in the dialog)."
msgstr ""
"Після створення документа відповідно до ваших налаштувань у вас буде біле "
"полотно (або полотно із кольором тла, який ви вибрали у діалоговому вікні)."

#: ../../user_manual/getting_started/starting_krita.rst:74
msgid "How to use brushes"
msgstr "Як користуватися пензлями"

#: ../../user_manual/getting_started/starting_krita.rst:76
msgid ""
"Now, just press on the canvas part. If everything's correct, you should be "
"able to draw on the canvas! The brush tool should be selected by default "
"when you start Krita, but if for some reason it is not, you can click on "
"this |toolfreehandbrush| icon from the toolbar and activate the brush tool."
msgstr ""
"Тепер, просто натисніть на полотно на сенсорному екрані або натисніть ліву "
"кнопку миші і проведіть вказівником на полотні. Ви можете малювати на "
"полотні! Типово, після запуску Krita буде вибрано інструмент пензля. Якщо це "
"з якоїсь причини не так, ви можете натиснути піктограму |toolfreehandbrush| "
"на панелі інструментів і активувати інструмент пензля."

#: ../../user_manual/getting_started/starting_krita.rst:79
msgid ""
"Of course, you'd want to use different brushes. On your right, there's a "
"docker named Brush Presets (or on top, press :kbd:`F6` to find this one) "
"with all these cute squares with pens and crayons."
msgstr ""
"Звичайно ж, ви можете скористатися іншими пензлями. Праворуч у вікні "
"програми буде показано бічну панель, яка називається «Набори пензлів» (або у "
"верхній частині вікна, натисніть :kbd:`F6`, щоб побачити її). На ній "
"розташовано усі ці чудові квадратики із перами та пензлями."

#: ../../user_manual/getting_started/starting_krita.rst:81
msgid ""
"If you want to tweak the presets, check the Brush Editor in the toolbar. You "
"can also access the Brush Editor with :kbd:`F5`."
msgstr ""
"Якщо ви хочете налаштувати набори пензлів, скористайтеся кнопкою редактора "
"пензлів на панелі інструментів. Крім того, вікно редактора пензлів можна "
"відкрити натисканням клавіші :kbd:`F5`."

#: ../../user_manual/getting_started/starting_krita.rst:84
msgid ".. image:: images/en/Krita_Brush_Preset_Docker.png"
msgstr ".. image:: images/en/Krita_Brush_Preset_Docker.png"

#: ../../user_manual/getting_started/starting_krita.rst:85
msgid ""
"Tick any of the squares to choose a brush, and then draw on the canvas. To "
"change color, click the triangle in the Advanced Color Selector docker."
msgstr ""
"Натисніть будь-який із квадратиків, щоб вибрати пензель, а потім намалюйте "
"ним щось на полотні. Щоб змінити колір, натисніть на трикутнику на бічній "
"панелі розширеного вибору кольору."

#: ../../user_manual/getting_started/starting_krita.rst:88
msgid "Erasing"
msgstr "Витирання"

#: ../../user_manual/getting_started/starting_krita.rst:90
msgid ""
"There are brush presets for erasing, but it is often faster to use the "
"eraser toggle. By toggling the :kbd:`E` key, your current brush switches "
"between erasing and painting."
msgstr ""
"Існують набори пензлів для витирання, але часто набагато швидшим є просте "
"використання перемикача витирання. Натисканням клавіші :kbd:`E` ви можете "
"перемикатися між режимами витирання і малювання для вашого пензля."

#: ../../user_manual/getting_started/starting_krita.rst:92
msgid ""
"This erasing method works with most of the tools. You can erase using the "
"line tool, rectangle tool, and even the gradient tool."
msgstr ""
"Цей метод витирання працює із більшістю інших інструментів. Ви можете "
"витирати за допомогою інструмента побудови прямих ліній, прямокутників, і "
"навіть за допомогою інструмента побудови градієнтів."

#: ../../user_manual/getting_started/starting_krita.rst:95
msgid "Saving and opening files"
msgstr "Збереження і відкриття файлів"

#: ../../user_manual/getting_started/starting_krita.rst:97
msgid ""
"Now, once you have figured out how to draw something in Krita, you may want "
"to save it. The save option is in the same place as it is in all other "
"computer programs: the top-menu of :guilabel:`File`, and then :guilabel:"
"`Save`."
msgstr ""
"Тепер, коли ви вже знаєте, як намалювати щось у Krita, ймовірно, вам "
"хочеться дізнатися про те, як зберегти намальоване. Пункт збереження даних "
"розташовано у тому самому місці, де його розташовано в усіх інших "
"комп'ютерних програмах: головне меню :guilabel:`Файл`, потім пункт :guilabel:"
"`Зберегти`."

#: ../../user_manual/getting_started/starting_krita.rst:99
msgid ""
"Select the folder you want to have your drawing, and select the file format "
"you want to use ('.kra' is Krita's default format, and will save "
"everything). And then hit :guilabel:`Save`. Some older versions of Krita "
"have a bug and require you to manually type the extension."
msgstr ""
"Виберіть теку, де має зберігатися ваше зображення, і виберіть формат файлів, "
"яким ви хочете скористатися («.kra» — типовий формат Krita, у якому можна "
"зберегти усі доступні програмі дані). Далі, натисніть кнопку :guilabel:"
"`Зберегти`. У деяких застарілих версіях Krita є вада, пов'язана із тим, що "
"вам доведеться вручну вказати суфікс назви файла."

#: ../../user_manual/getting_started/starting_krita.rst:101
msgid ""
"If you want to show off your image on the internet, check out the :ref:"
"`saving_for_the_web` tutorial."
msgstr ""
"Якщо ви маєте намір розмістити ваше зображення у інтернеті, ознайомтеся із "
"вмістом розділу :ref:`saving_for_the_web`."

#: ../../user_manual/getting_started/starting_krita.rst:103
msgid ""
"Check out :ref:`navigation` for further basic information, :ref:"
"`basic_concepts` for an introduction as Krita as a medium, or just go out "
"and explore Krita!"
msgstr ""
"Прочитайте розділ :ref:`navigation`, щоб дізнатися більше про основі роботи "
"у програмі, розділ :ref:`basic_concepts`, який є вступним до справжнього "
"вивчення прийомів роботи у Krita, або просто натискайте кнопки і вчіться "
"роботі у Krita на прикладах!"
