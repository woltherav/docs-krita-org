# Translation of docs_krita_org_general_concepts___projection___orthographic_oblique.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___projection___orthographic_oblique\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 03:46+0200\n"
"PO-Revision-Date: 2019-04-03 14:58+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection-cube_01.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_01.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection-cube_02.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_02.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection-cube_03.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_03.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection-cube_04.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_04.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection-cube_05.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_05.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection-cube_06.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_06.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_01.png"
msgstr ".. image:: images/en/category_projection/projection_image_01.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_02.png"
msgstr ".. image:: images/en/category_projection/projection_image_02.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_03.png"
msgstr ".. image:: images/en/category_projection/projection_image_03.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_04.png"
msgstr ".. image:: images/en/category_projection/projection_image_04.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_05.png"
msgstr ".. image:: images/en/category_projection/projection_image_05.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_06.png"
msgstr ".. image:: images/en/category_projection/projection_image_06.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_07.png"
msgstr ".. image:: images/en/category_projection/projection_image_07.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_08.png"
msgstr ".. image:: images/en/category_projection/projection_image_08.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_09.png"
msgstr ".. image:: images/en/category_projection/projection_image_09.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_10.png"
msgstr ".. image:: images/en/category_projection/projection_image_10.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_11.png"
msgstr ".. image:: images/en/category_projection/projection_image_11.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_12.png"
msgstr ".. image:: images/en/category_projection/projection_image_12.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_13.png"
msgstr ".. image:: images/en/category_projection/projection_image_13.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_image_14.png"
msgstr ".. image:: images/en/category_projection/projection_image_14.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/en/category_projection/projection_animation_01.gif"
msgstr ".. image:: images/en/category_projection/projection_animation_01.gif"

#: ../../general_concepts/projection/orthographic_oblique.rst:1
msgid "Orthographics and oblique projection."
msgstr "Ортографічна і косокутна проекції."

#: ../../general_concepts/projection/orthographic_oblique.rst:10
msgid "So let's start with the basics..."
msgstr "Почнімо з основ…"

#: ../../general_concepts/projection/orthographic_oblique.rst:16
msgid "Orthographic"
msgstr "Ортографічна проекція"

#: ../../general_concepts/projection/orthographic_oblique.rst:18
msgid ""
"Despite the fancy name, you probably know what orthographic is. It is a "
"schematic representation of an object, draw undeformed. Like the following "
"example:"
msgstr ""
"Хоча назва доволі незвична, ви, ймовірно, знаєте, що означає слово "
"«ортографічна». Це схематичне відтворення об'єкта, намальоване без "
"деформації. Як у цьому прикладі:"

#: ../../general_concepts/projection/orthographic_oblique.rst:23
msgid ""
"This is a rectangle. We have a front, top and side view. Put into "
"perspective it should look somewhat like this:"
msgstr ""
"Це паралелепіпед. Ми маємо вид спереду, вид згори і вид збоку. У перспективі "
"це має виглядати якось так:"

#: ../../general_concepts/projection/orthographic_oblique.rst:28
msgid ""
"While orthographic representations are kinda boring, they're also a good "
"basis to start with when you find yourself in trouble with a pose. But we'll "
"get to that in a bit."
msgstr ""
"Хоча ортографічні відтворення є дещо класичними, вони є доброю основою, якщо "
"у вас виникають проблеми із позою. Але це ми обговоримо окремо."

#: ../../general_concepts/projection/orthographic_oblique.rst:33
msgid "Oblique"
msgstr "Косокутна проекція"

#: ../../general_concepts/projection/orthographic_oblique.rst:35
msgid ""
"So, if we can say that the front view is the viewer looking at the front, "
"and the side view is the viewer directly looking at the side. (The "
"perpendicular line being the view plane it is projected on)"
msgstr ""
"Отже, якщо можна так сказати, вид спереду — це коли ви дивитеся на об'єкт "
"спереду, а вид збоку — це коли ви дивитеся на об'єкт безпосередньо збоку. "
"(Лінія перпендикуляра визначає площину перегляду, на яку виконується "
"проектування.)"

#: ../../general_concepts/projection/orthographic_oblique.rst:40
msgid "Then we can get a half-way view from looking from an angle, no?"
msgstr ""
"Далі, ми можемо отримати якийсь проміжний варіант перегляду під певним "
"кутом, чи не так?"

#: ../../general_concepts/projection/orthographic_oblique.rst:45
msgid "If we do that for a lot of different sides…"
msgstr "Якщо ми зробимо це для багатьох різних кутів…"

#: ../../general_concepts/projection/orthographic_oblique.rst:50
msgid "And we line up the sides we get a…"
msgstr "І упорядкуємо отримані зображення, ми отримаємо…"

#: ../../general_concepts/projection/orthographic_oblique.rst:55
msgid ""
"But cubes are boring. I am suspecting that projection is so ignored because "
"no tutorial applies it to an object where you actually might NEED "
"projection. Like a face."
msgstr ""
"Але кубики надто банальні. Автор підозрює, що питання проекцій ігнорують, "
"оскільки жодні настанови з проекцій не стосуються об'єктів, для яких вам "
"справді потрібна проекція. Зокрема обличчя."

#: ../../general_concepts/projection/orthographic_oblique.rst:57
msgid "First, let's prepare our front and side views:"
msgstr "Спочатку, приготуємо наші види спереду і збоку:"

#: ../../general_concepts/projection/orthographic_oblique.rst:62
msgid ""
"I always start with the side, and then extrapolate the front view from it. "
"Because you are using Krita, set up two parallel rulers, one vertical and "
"the other horizontal. To snap them perfectly, drag one of the nodes after "
"you have made the ruler, and press :kbd:`Shift` to snap it horizontal or "
"vertical. In 3.0, you can also snap them to the image borders if you have :"
"menuselection:`Snap Image Bounds` active via :kbd:`Shift` + :kbd:`S`"
msgstr ""
"Автор завжди розпочинає з виду збоку, а потім екстраполює вид спереду на "
"його основі. Оскільки ми користуємося Krita, налаштуємо дві паралельних "
"лінійки, одну вертикальну і одну горизонтальну. Для ідеального прилипання "
"перетягніть один з вузлів після створення лінійки і натисніть клавішу :kbd:"
"`Shift`, щоб він прилип до горизонтальної або вертикальної лінійки. "
"Починаючи з версії 3.0, ви можете також скористатися прилипанням до країв "
"зображення, якщо активовано :menuselection:`Прилипати до меж зображень`."

#: ../../general_concepts/projection/orthographic_oblique.rst:64
msgid ""
"Then, by moving the mirror to the left, you can design a front view from the "
"side view, while the parallel preview line helps you with aligning the eyes "
"(which in the above screenshot are too low)."
msgstr ""
"Далі, пересуваючи дзеркало ліворуч, ви можете створити вид спереду з виду "
"збоку, а паралельна лінія попереднього перегляду допоможе вам із "
"вирівнюванням очей (які на наведеному вище знімку розташовано надто низько)."

#: ../../general_concepts/projection/orthographic_oblique.rst:66
msgid "Eventually, you should have something like this:"
msgstr "Нарешті, у вас має вийти щось таке:"

#: ../../general_concepts/projection/orthographic_oblique.rst:71
msgid "And of course, let us not forget the top, it's pretty important:"
msgstr "І, звичайно ж, не забуваймо про вид згори, — він є дуже важливим:"

#: ../../general_concepts/projection/orthographic_oblique.rst:78
msgid ""
"When you are using Krita, you can just use transform masks to rotate the "
"side view for drawing the top view."
msgstr ""
"Коли ви користуєтеся Krita, ви можете просто скористатися масками "
"перетворення для обертання виду збоку для малювання виду згори."

#: ../../general_concepts/projection/orthographic_oblique.rst:80
msgid ""
"The top view works as a method for debugging your orthos as well. If we take "
"the red line to figure out the orthographics from, we see that our eyes are "
"obviously too inset. Let's move them a bit more forward, to around the nose."
msgstr ""
"Вид згори може також бути способом діагностики ваших ортографічних проекцій. "
"Якщо взяти червону для визначення початкової точки ортографічної проекції, "
"можна бачити, що наші очі є, очевидно, розташованими надто глибоко. "
"Пересуньмо їх трохи вперед до кінчика носу."

#: ../../general_concepts/projection/orthographic_oblique.rst:85
msgid ""
"If you want to do precision position moving in the tool options docker, just "
"select 'position' and the input box for the X. Pressing down then moves the "
"transformed selection left. With Krita 3.0 you can just use the move tool "
"for this and the arrow keys. Using transform here can be more convenient if "
"you also have to squash and stretch an eye."
msgstr ""
"Якщо ви хочете виконати точне пересування на бічній панелі параметрів "
"інструмента, просто виберіть «Розташування» та поле введення для X. "
"Натискання клавіші зі стрілкою вниз пересуває перетворене позначення "
"ліворуч. У версіях, починаючи з Krita 3.0, ви можете просто скористатися для "
"цього інструментом пересування та клавішами зі стрілочками. Використання тут "
"перетворення може бути зручнішим, якщо ви також втиснете і розтягнете око."

#: ../../general_concepts/projection/orthographic_oblique.rst:90
msgid "We fix the top view now. Much better."
msgstr "Тепер, ми виправимо вид згори. Набагато краще."

#: ../../general_concepts/projection/orthographic_oblique.rst:92
msgid ""
"For faces, the multiple slices are actually pretty important. So important "
"even, that I have decided we should have these slices on separate layers. "
"Thankfully, I chose to color them, so all we need to do is go to :"
"menuselection:`Layer --> Split Layer` ."
msgstr ""
"Для облич, насправді, доволі важливими є декілька перерізів. Настільки "
"важливими, що автор вирішив розмістити ці перерізи у окремих шарах. На "
"щастя, він вибрав для них колір, отже, лишилося лише скористатися пунктом "
"меню :menuselection:`Шар --> Розділити шар`."

#: ../../general_concepts/projection/orthographic_oblique.rst:98
msgid ""
"This'll give you a few awkwardly named layers… rename them by selecting all "
"and mass changing the name in the properties editor:"
msgstr ""
"Це дасть нам декілька шарів із доволі незграбними назвами. Перейменуємо їх, "
"позначивши усі шари, і пакетно змінивши назви у редакторі властивостей:"

#: ../../general_concepts/projection/orthographic_oblique.rst:103
msgid "So, after some cleanup, we should have the following:"
msgstr "Отже, після певного чищення, у нас має вийти щось таке:"

#: ../../general_concepts/projection/orthographic_oblique.rst:108
msgid "Okay, now we're gonna use animation for the next bit."
msgstr "Гаразд, тепер ми хочемо створити анімацію."

#: ../../general_concepts/projection/orthographic_oblique.rst:110
msgid "Set it up as follows:"
msgstr "Налаштуємо усе так:"

#: ../../general_concepts/projection/orthographic_oblique.rst:115
msgid ""
"Both front view and side view are set up as 'visible in timeline' so we can "
"always see them."
msgstr ""
"Вид спереду і вид збоку налаштовано як «видимі на монтажному столі», отже ми "
"їх завжди бачитимемо."

#: ../../general_concepts/projection/orthographic_oblique.rst:116
msgid ""
"Front view has its visible frame on frame 0 and an empty frame on frame 23."
msgstr "Для виду спереду видимим кадром є кадр 0, а порожнім кадром є кадр 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:117
msgid ""
"Side view has its visible frame on frame 23 and an empty view on frame 0."
msgstr "Для виду збоку видимим кадром є кадр 23, а порожнім кадром є кадр 0."

#: ../../general_concepts/projection/orthographic_oblique.rst:118
msgid "The end of the animation is set to 23."
msgstr "Кінець анімації встановлено на кадрі 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:123
msgid ""
"Krita can't animate a transformation on multiple layers on multiple frames "
"yet, so let's just only transform the top layer. Add a semi-transparent "
"layer where we draw the guidelines."
msgstr ""
"У Krita ще не передбачено анімування перетворення над декількома шарами у "
"декількох кадрах, тому ми виконаємо перетворення лише верхнього шару. Додамо "
"напівпрозорий шар, де малюватимемо напрямні."

#: ../../general_concepts/projection/orthographic_oblique.rst:125
msgid ""
"Now, select frame 11 (halfway), add new frames from front view, side view "
"and the guidelines. And turn on the onion skin by toggling the lamp symbols. "
"We copy the frame for the top view and use the transform tool to rotate it "
"45°."
msgstr ""
"Тепер позначимо кадр 11 (посередині), додамо нові кадри з виду спереду, виду "
"збоку та напрямні. Увімкнемо кальку, натиснувши піктограми лампочок. "
"Скопіюємо кадр для виду згори і скористаємося інструментом перетворення для "
"його обертання на 45°."

#: ../../general_concepts/projection/orthographic_oblique.rst:130
msgid "So, we draw our vertical guides again and determine a in-between..."
msgstr ""
"Отже, малюємо наші вертикальні напрямні знову і визначаємо проміжні кадри…"

#: ../../general_concepts/projection/orthographic_oblique.rst:135
msgid ""
"This is about how far you can get with only the main slice, so rotate the "
"rest as well."
msgstr ""
"Цей і усе, що ви можете отримати лише з основним перерізом, тому "
"обертатимемо і решту."

#: ../../general_concepts/projection/orthographic_oblique.rst:140
msgid "And just like with the cube, we do this for all slices…"
msgstr "І, як і з кубом, ми робимо це для усіх перерізів…"

#: ../../general_concepts/projection/orthographic_oblique.rst:145
msgid ""
"Eventually, if you have the top slices rotate every frame with 15°, you "
"should be able to make a turn table, like this:"
msgstr ""
"Нарешті, якщо у вас перерізи згоди обертаються на 15° кожного кадру, ви "
"можете побудувати обертальний круг, ось так:"

#: ../../general_concepts/projection/orthographic_oblique.rst:150
msgid ""
"Because our boy here is fully symmetrical, you can just animate one side and "
"flip the frames for the other half."
msgstr ""
"Оскільки наш хлопчик повністю симетричний, достатньо анімувати один бік і "
"віддзеркалити кадри для іншої половини."

#: ../../general_concepts/projection/orthographic_oblique.rst:152
msgid ""
"While it is not necessary to follow all the steps in the theory section to "
"understand the tutorial, I do recommend making a turn table sometime. It "
"teaches you a lot about drawing 3/4th faces."
msgstr ""
"Хоча і немає потреби виконувати усі ці кроки у теоретичній частині для того, "
"щоб зрозуміти тему цього підручника, рекомендуємо все ж створити анімацію "
"обертального кругу. Це багато чому навчить вас у малюванні обернути на три "
"чверті облич."

#: ../../general_concepts/projection/orthographic_oblique.rst:154
msgid "How about… we introduce the top view into the drawing itself?"
msgstr "Як щодо того, щоб… впровадити вид згори до самого малюнка?"
