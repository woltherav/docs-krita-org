msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/en/brush-tips/Brushtip-Rainbow.png\n"
"   :alt: selecting fill circle for brush tip"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/en/brush-tips/Brushtip-Rainbow_2.png\n"
"   :alt: toggle hue in the brush parameter"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/en/brush-tips/Brushtip-Rainbow_3.png\n"
"   :alt: select distance parameter for the hue"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:1
msgid "A tutorial about making rainbow brush in krita"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:13
msgid "Brush-tips:Rainbow Brush"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:16
msgid "Question"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:18
msgid "**Hello, there is a way to paint with rainbow on Krita?**"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:20
msgid "Yes there is."
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:22
msgid "First, select the fill_circle:"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:27
msgid "Then, press :kbd:`F5` to open the brush editor, and toggle **Hue**."
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:32
msgid "This should allow you to change the color depending on the pressure."
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:34
msgid ""
"The brightness of the rainbow is relative to the color of the currently "
"selected color, so make sure to select bright saturated colors for a bright "
"rainbow!"
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:36
msgid ""
"Uncheck **Pressure** and check **Distance** to make the rainbow paint itself "
"over distance. The slider below can be |mouseright| to change the value with "
"keyboard input."
msgstr ""

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:41
msgid "When you are satisfied, give the brush a new name and save it."
msgstr ""
