# Vincent Pinon <vpinon@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-25 03:36+0100\n"
"PO-Revision-Date: 2019-03-12 00:15+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:1
msgid "Krita's polygon tool reference."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:15
msgid "Polygon Tool"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:17
msgid "|toolpolygon|"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:19
msgid ""
"With this tool you can draw polygons. Click the |mouseleft| to indicate the "
"starting point and successive vertices, then double-click or press :kbd:"
"`Enter` to connect the last vertex to the starting point."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:21
msgid ":kbd:`Shift +` |mouseleft| undoes the last clicked point."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:24
msgid "Tool Options"
msgstr "Préférences d'outils"
