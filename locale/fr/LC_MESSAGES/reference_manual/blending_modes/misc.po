msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-02-27 01:12+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""

# #-#-#-#-#  katecloseexceptplugin.po (katepart4)  #-#-#-#-#
# | msgctxt "Language"
# | msgid "Cisco"
# #-#-#-#-#  ktexteditor5.po (katepart4)  #-#-#-#-#
# | msgctxt "Language"
# | msgid "Cisco"
# #-#-#-#-#  akregator.po (akregator)  #-#-#-#-#
# | msgctxt "Language"
# | msgid "Cisco"
#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr "Divers"

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr "Combiner Normal map"

#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <http://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/Blending_modes_Copy_Sample_image_with_dots."
"png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr ""
