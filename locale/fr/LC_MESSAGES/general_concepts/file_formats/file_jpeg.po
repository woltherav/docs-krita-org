msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-25 03:36+0100\n"
"PO-Revision-Date: 2019-02-27 00:59+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../general_concepts/file_formats/file_jpeg.rst:1
msgid "The JPEG file format as exported by Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:16
msgid "\\*.jpg"
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:18
msgid ""
".jpg, .jpeg or .jpeg2000 are a family of file-formats designed to encode "
"photographs."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:20
msgid ""
"Photographs have the problem that they have a lot of little gradients, which "
"means that you cannot index the file like you can with :ref:`file_gif` and "
"expect the result to look good. What jpeg instead does is that it converts "
"the file to a perceptual color space (:ref:`YCrCb <model_ycrcb>`), and then "
"compresses the channels that encode the colors, while keeping the channel "
"that holds information about the relative lightness uncompressed. This works "
"really well because human eye-sight is not as sensitive to colorfulness as "
"it is to relative lightness. Jpeg also uses other :ref:`lossy "
"<lossy_compression>` compression techniques, like using cosine waves to "
"describe image contrasts."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:22
msgid ""
"However, it does mean that jpeg should be used in certain cases. For images "
"with a lot of gradients, like full scale paintings, jpeg performs better "
"than :ref:`file_png` and :ref:`file_gif`."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:24
msgid ""
"But for images with a lot of sharp contrasts, like text and comic book "
"styles, png is a much better choice despite a larger file size. For "
"grayscale images, png and gif will definitely be more efficient."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:26
msgid ""
"Because jpeg uses lossy compression, it is not advised to save over the same "
"jpeg multiple times. The lossy compression will cause the file to reduce in "
"quality each time you save it. This is a fundamental problem with lossy "
"compression methods. Instead use a lossless file format, or a working file "
"format while you are working on the image."
msgstr ""
