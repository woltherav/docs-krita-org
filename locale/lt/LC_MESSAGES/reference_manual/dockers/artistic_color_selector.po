# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-17 03:39+0200\n"
"PO-Revision-Date: 2019-04-17 03:39+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:1
msgid "Overview of the artistic color selector docker."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:16
msgid "Artist Color Selector Docker"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:19
msgid ".. image:: images/en/Krita_Artistic_Color_Selector_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:20
msgid ""
"A round selector that tries to give you the tools to select colors ramps "
"efficiently."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:23
msgid "Preference"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:25
msgid ""
"Set the color model used by the selector, as well as the amount of segments."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:28
msgid "Reset"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:30
msgid "Reset the selector to a default stage."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:33
msgid "Absolute"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:35
msgid ""
"This changes the algorithm around so it gives proper values for the gray. "
"Without absolute, it'll use HSV values for gray to the corresponding hue and "
"lightness."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:38
msgid "Usage"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:40
msgid ""
"|mouseleft| the swatches to change the foreground color. Use |mouseright| + "
"Drag to shift the alignment of the selector swatches within a specific "
"saturation ring. Use |mouseleft| + Drag to shift the alignment of all "
"swatches."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:43
msgid "This selector does not update on change of foreground color."
msgstr ""
