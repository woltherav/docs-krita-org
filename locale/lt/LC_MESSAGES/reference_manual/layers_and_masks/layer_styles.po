# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-14 03:27+0100\n"
"PO-Revision-Date: 2019-03-14 03:27+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:1
msgid "How to use layer styles in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:17
msgid "Layer Styles"
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:19
msgid ""
"Layer styles are effects that are added on top of your layer. They are "
"editable and can easily be toggled on and off. To add a layer style to a "
"layer go to :menuselection:`Layer --> Layer Style`. You can also right-click "
"a layer to access the layer styles."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:22
msgid ""
"When you have the layer styles window up, make sure that the :guilabel:"
"`Enable Effects` item is checked."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:24
msgid ""
"There are a variety of effects and styles you can apply to a layer. When you "
"add a style, your layer docker will show an extra \"Fx\" icon. This allows "
"you to toggle the layer style effects on and off."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:30
msgid ""
"This feature was added to increase support for :program:`Adobe Photoshop`. "
"The features that are included mirror what that application supports."
msgstr ""
